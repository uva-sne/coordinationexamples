# Coordination Examples

Various DAG applications that illustrate Cecile / Yasmin usage. Requires `cecile` is on your `$PATH`.

- [Get Cecile (coordination compiler)](https://bitbucket.org/uva-sne/coordinationcompiler/src/master/)
- [Get Yasmin (runtime environment)](https://bitbucket.org/uva-sne/coordinationruntime/src/master/)


## Yasmin examples

Yasmin is a real-time runtime environment, which can be targetted by the `<generator id="yasmin" ../>` compiler pass. In the `yasmin` folder, there are various examples

### Compiling the examples

First, clone the Cecile and Yasmin projects. Then, from the "yasmin" folder
```sh
coordinationexamples/yasmin$ make clean
~/coordinationexamples/yasmin$ make all YASMIN_PATH=/home/lukasuva/Development/Coordination/coordinationruntime
```

Individual parts can also be built (but not cleaned) using
```sh
coordinationexamples/yasmin$ make clean
coordinationexamples/yasmin$ make ForkJoin YASMIN_PATH=/home/lukasuva/Development/Coordination/coordinationruntime
```

Don't forget to run clean every single time.

### SingleTask
Simple example with just a single task

### ForkJoin
More involved example showing broadcast and communication

### DataRace
Showcase of what happens when the reported WCET is violated (behavior is undefined)
