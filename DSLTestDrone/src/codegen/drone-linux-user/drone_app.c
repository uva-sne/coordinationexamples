
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <semaphore.h> 
#include <signal.h>
#include <limits.h>

%INCLUDES%

#define errExit(msg) { perror(msg); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) errExit(msg)

pthread_barrier_t main_barrier;
sem_t main_ack;
sem_t __start__lock;

sem_t ImageCapture_lock;
sem_t GroundSpeed_lock;
sem_t Encryption_lock;
sem_t ObjectDetector_lock;
sem_t SaveVideo_lock;
sem_t Decision_lock;
sem_t SendMessage_lock;



pthread_t th_ImageCapture__v1;
void * __ImageCapture__v1__(void* unused)
{
    int status;
    
    status = sem_init(&ImageCapture_lock, 0, 0);
    if(status != 0) {
        perror("sem_init(&ImageCapture_lock, 0, 0)");
        return NULL;
    }

    // ack main we have locked successors
    // if not done, there is no guaranty that this thread actually get executed 
    // before the barrier is destroyed
    status = sem_post(&main_ack);
    if(status != 0) {
        perror("sem_post(&main_ack)");
        return NULL;
    }

    // wait for every body
    status = pthread_barrier_wait(&main_barrier);
    if(status != 0 && status != PTHREAD_BARRIER_SERIAL_THREAD) {
        perror("pthread_barrier_wait(&main_barrier)");
        return NULL;
    }

    for(size_t __i__=0 ; __i__ < 3 ; ++__i__) {
        
    status = sem_wait(&__start__lock);
    if(status != 0) {
        perror("sem_wait(&__start__lock)");
        return NULL;
    }


        ();

        

        for(size_t i = 0 ; i < 3 ; ++i) {
            status = sem_post(&ImageCapture_lock);
            if(status != 0) {
                perror("sem_post(&ImageCapture_lock)");
                return NULL;
            }
        }
    }

    return NULL;
}

pthread_t th_GroundSpeed__v1;
void * __GroundSpeed__v1__(void* unused)
{
    int status;
    
    status = sem_init(&GroundSpeed_lock, 0, 0);
    if(status != 0) {
        perror("sem_init(&GroundSpeed_lock, 0, 0)");
        return NULL;
    }

    // ack main we have locked successors
    // if not done, there is no guaranty that this thread actually get executed 
    // before the barrier is destroyed
    status = sem_post(&main_ack);
    if(status != 0) {
        perror("sem_post(&main_ack)");
        return NULL;
    }

    // wait for every body
    status = pthread_barrier_wait(&main_barrier);
    if(status != 0 && status != PTHREAD_BARRIER_SERIAL_THREAD) {
        perror("pthread_barrier_wait(&main_barrier)");
        return NULL;
    }

    for(size_t __i__=0 ; __i__ < 3 ; ++__i__) {
        
    status = sem_wait(&ImageCapture_lock);
    if(status != 0) {
        perror("sem_wait(&ImageCapture_lock)");
        return NULL;
    }


        ();

        

        for(size_t i = 0 ; i < 1 ; ++i) {
            status = sem_post(&GroundSpeed_lock);
            if(status != 0) {
                perror("sem_post(&GroundSpeed_lock)");
                return NULL;
            }
        }
    }

    return NULL;
}

pthread_t th_Encryption__RSA;
void * __Encryption__RSA__(void* unused)
{
    int status;
    
    status = sem_init(&Encryption_lock, 0, 0);
    if(status != 0) {
        perror("sem_init(&Encryption_lock, 0, 0)");
        return NULL;
    }

    // ack main we have locked successors
    // if not done, there is no guaranty that this thread actually get executed 
    // before the barrier is destroyed
    status = sem_post(&main_ack);
    if(status != 0) {
        perror("sem_post(&main_ack)");
        return NULL;
    }

    // wait for every body
    status = pthread_barrier_wait(&main_barrier);
    if(status != 0 && status != PTHREAD_BARRIER_SERIAL_THREAD) {
        perror("pthread_barrier_wait(&main_barrier)");
        return NULL;
    }

    for(size_t __i__=0 ; __i__ < 3 ; ++__i__) {
        
    status = sem_wait(&ImageCapture_lock);
    if(status != 0) {
        perror("sem_wait(&ImageCapture_lock)");
        return NULL;
    }


        ();

        

        for(size_t i = 0 ; i < 1 ; ++i) {
            status = sem_post(&Encryption_lock);
            if(status != 0) {
                perror("sem_post(&Encryption_lock)");
                return NULL;
            }
        }
    }

    return NULL;
}

pthread_t th_ObjectDetector__TinyDarknet;
void * __ObjectDetector__TinyDarknet__(void* unused)
{
    int status;
    
    status = sem_init(&ObjectDetector_lock, 0, 0);
    if(status != 0) {
        perror("sem_init(&ObjectDetector_lock, 0, 0)");
        return NULL;
    }

    // ack main we have locked successors
    // if not done, there is no guaranty that this thread actually get executed 
    // before the barrier is destroyed
    status = sem_post(&main_ack);
    if(status != 0) {
        perror("sem_post(&main_ack)");
        return NULL;
    }

    // wait for every body
    status = pthread_barrier_wait(&main_barrier);
    if(status != 0 && status != PTHREAD_BARRIER_SERIAL_THREAD) {
        perror("pthread_barrier_wait(&main_barrier)");
        return NULL;
    }

    for(size_t __i__=0 ; __i__ < 3 ; ++__i__) {
        
    status = sem_wait(&ImageCapture_lock);
    if(status != 0) {
        perror("sem_wait(&ImageCapture_lock)");
        return NULL;
    }


        ();

        

        for(size_t i = 0 ; i < 1 ; ++i) {
            status = sem_post(&ObjectDetector_lock);
            if(status != 0) {
                perror("sem_post(&ObjectDetector_lock)");
                return NULL;
            }
        }
    }

    return NULL;
}

pthread_t th_SaveVideo__v1;
void * __SaveVideo__v1__(void* unused)
{
    int status;
    
    status = sem_init(&SaveVideo_lock, 0, 0);
    if(status != 0) {
        perror("sem_init(&SaveVideo_lock, 0, 0)");
        return NULL;
    }

    // ack main we have locked successors
    // if not done, there is no guaranty that this thread actually get executed 
    // before the barrier is destroyed
    status = sem_post(&main_ack);
    if(status != 0) {
        perror("sem_post(&main_ack)");
        return NULL;
    }

    // wait for every body
    status = pthread_barrier_wait(&main_barrier);
    if(status != 0 && status != PTHREAD_BARRIER_SERIAL_THREAD) {
        perror("pthread_barrier_wait(&main_barrier)");
        return NULL;
    }

    for(size_t __i__=0 ; __i__ < 3 ; ++__i__) {
        
    status = sem_wait(&Encryption_lock);
    if(status != 0) {
        perror("sem_wait(&Encryption_lock)");
        return NULL;
    }


        ();

        

        for(size_t i = 0 ; i < 0 ; ++i) {
            status = sem_post(&SaveVideo_lock);
            if(status != 0) {
                perror("sem_post(&SaveVideo_lock)");
                return NULL;
            }
        }
    }

    return NULL;
}

pthread_t th_Decision__v1;
void * __Decision__v1__(void* unused)
{
    int status;
    
    status = sem_init(&Decision_lock, 0, 0);
    if(status != 0) {
        perror("sem_init(&Decision_lock, 0, 0)");
        return NULL;
    }

    // ack main we have locked successors
    // if not done, there is no guaranty that this thread actually get executed 
    // before the barrier is destroyed
    status = sem_post(&main_ack);
    if(status != 0) {
        perror("sem_post(&main_ack)");
        return NULL;
    }

    // wait for every body
    status = pthread_barrier_wait(&main_barrier);
    if(status != 0 && status != PTHREAD_BARRIER_SERIAL_THREAD) {
        perror("pthread_barrier_wait(&main_barrier)");
        return NULL;
    }

    for(size_t __i__=0 ; __i__ < 3 ; ++__i__) {
        
    status = sem_wait(&GroundSpeed_lock);
    if(status != 0) {
        perror("sem_wait(&GroundSpeed_lock)");
        return NULL;
    }

    status = sem_wait(&ObjectDetector_lock);
    if(status != 0) {
        perror("sem_wait(&ObjectDetector_lock)");
        return NULL;
    }


        ();

        

        for(size_t i = 0 ; i < 1 ; ++i) {
            status = sem_post(&Decision_lock);
            if(status != 0) {
                perror("sem_post(&Decision_lock)");
                return NULL;
            }
        }
    }

    return NULL;
}

pthread_t th_SendMessage__v1;
void * __SendMessage__v1__(void* unused)
{
    int status;
    
    status = sem_init(&SendMessage_lock, 0, 0);
    if(status != 0) {
        perror("sem_init(&SendMessage_lock, 0, 0)");
        return NULL;
    }

    // ack main we have locked successors
    // if not done, there is no guaranty that this thread actually get executed 
    // before the barrier is destroyed
    status = sem_post(&main_ack);
    if(status != 0) {
        perror("sem_post(&main_ack)");
        return NULL;
    }

    // wait for every body
    status = pthread_barrier_wait(&main_barrier);
    if(status != 0 && status != PTHREAD_BARRIER_SERIAL_THREAD) {
        perror("pthread_barrier_wait(&main_barrier)");
        return NULL;
    }

    for(size_t __i__=0 ; __i__ < 3 ; ++__i__) {
        
    status = sem_wait(&Decision_lock);
    if(status != 0) {
        perror("sem_wait(&Decision_lock)");
        return NULL;
    }


        ();

        

        for(size_t i = 0 ; i < 0 ; ++i) {
            status = sem_post(&SendMessage_lock);
            if(status != 0) {
                perror("sem_post(&SendMessage_lock)");
                return NULL;
            }
        }
    }

    return NULL;
}


static void handler_timer_start_iteration(int sig, siginfo_t *si, void *uc) {
    for(size_t i=0 ; i < 1 ; ++i)
        sem_post(&__start__lock);
}

int main(char **argv, int argc) 
{
    int status;
    pthread_attr_t attr;
    struct sched_param schedattr;
    struct sigaction sa;
    struct sigevent te;
    struct itimerspec its;
    timer_t timer;

    status = pthread_barrier_init(&main_barrier, NULL, 7+1); //+1 -> main
    tryExit(status, "pthread_barrier_init(&main_barrier, NULL, 7+1)");

    status = sem_init(&main_ack, 0, 0);
    tryExit(status, "sem_init(&main_ack, 0, 0)");
    status = sem_init(&__start__lock, 0, 0);
    tryExit(status, "sem_init(&__start__lock, 0, 0)");

    
    status = pthread_attr_init(&attr);
    tryExit(status, "th_ImageCapture__v1 - pthread_attr_init(&attr)");
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    tryExit(status, "th_ImageCapture__v1 - pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED)");
    status = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    tryExit(status, "th_ImageCapture__v1 - pthread_attr_setschedpolicy(&attr, SCHED_FIFO)");
    schedattr.sched_priority = 1;
    status = pthread_attr_setschedparam(&attr, &schedattr);
    tryExit(status, "th_ImageCapture__v1 - pthread_attr_setschedparam(&attr, &schedattr)");
    status = pthread_attr_getschedparam(&attr, &schedattr);
    tryExit(status, "th_ImageCapture__v1 - pthread_attr_getschedparam(&attr, &schedattr)");
    status = pthread_create(&th_ImageCapture__v1, &attr, &__ImageCapture__v1__, NULL);
    tryExit(status, "pthread_create(&th_ImageCapture__v1, &attr, &__ImageCapture__v1__, NULL)\n");
    // ack just created thread
    status = sem_wait(&main_ack);
    tryExit(status, "th_ImageCapture__v1 - sem_wait(&main_ack)");
    status = pthread_attr_destroy(&attr);
    tryExit(status, "th_ImageCapture__v1 - pthread_attr_destroy(&attr)");

    status = pthread_attr_init(&attr);
    tryExit(status, "th_GroundSpeed__v1 - pthread_attr_init(&attr)");
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    tryExit(status, "th_GroundSpeed__v1 - pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED)");
    status = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    tryExit(status, "th_GroundSpeed__v1 - pthread_attr_setschedpolicy(&attr, SCHED_FIFO)");
    schedattr.sched_priority = 2;
    status = pthread_attr_setschedparam(&attr, &schedattr);
    tryExit(status, "th_GroundSpeed__v1 - pthread_attr_setschedparam(&attr, &schedattr)");
    status = pthread_attr_getschedparam(&attr, &schedattr);
    tryExit(status, "th_GroundSpeed__v1 - pthread_attr_getschedparam(&attr, &schedattr)");
    status = pthread_create(&th_GroundSpeed__v1, &attr, &__GroundSpeed__v1__, NULL);
    tryExit(status, "pthread_create(&th_GroundSpeed__v1, &attr, &__GroundSpeed__v1__, NULL)\n");
    // ack just created thread
    status = sem_wait(&main_ack);
    tryExit(status, "th_GroundSpeed__v1 - sem_wait(&main_ack)");
    status = pthread_attr_destroy(&attr);
    tryExit(status, "th_GroundSpeed__v1 - pthread_attr_destroy(&attr)");

    status = pthread_attr_init(&attr);
    tryExit(status, "th_Encryption__RSA - pthread_attr_init(&attr)");
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    tryExit(status, "th_Encryption__RSA - pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED)");
    status = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    tryExit(status, "th_Encryption__RSA - pthread_attr_setschedpolicy(&attr, SCHED_FIFO)");
    schedattr.sched_priority = 2;
    status = pthread_attr_setschedparam(&attr, &schedattr);
    tryExit(status, "th_Encryption__RSA - pthread_attr_setschedparam(&attr, &schedattr)");
    status = pthread_attr_getschedparam(&attr, &schedattr);
    tryExit(status, "th_Encryption__RSA - pthread_attr_getschedparam(&attr, &schedattr)");
    status = pthread_create(&th_Encryption__RSA, &attr, &__Encryption__RSA__, NULL);
    tryExit(status, "pthread_create(&th_Encryption__RSA, &attr, &__Encryption__RSA__, NULL)\n");
    // ack just created thread
    status = sem_wait(&main_ack);
    tryExit(status, "th_Encryption__RSA - sem_wait(&main_ack)");
    status = pthread_attr_destroy(&attr);
    tryExit(status, "th_Encryption__RSA - pthread_attr_destroy(&attr)");

    status = pthread_attr_init(&attr);
    tryExit(status, "th_ObjectDetector__TinyDarknet - pthread_attr_init(&attr)");
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    tryExit(status, "th_ObjectDetector__TinyDarknet - pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED)");
    status = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    tryExit(status, "th_ObjectDetector__TinyDarknet - pthread_attr_setschedpolicy(&attr, SCHED_FIFO)");
    schedattr.sched_priority = 2;
    status = pthread_attr_setschedparam(&attr, &schedattr);
    tryExit(status, "th_ObjectDetector__TinyDarknet - pthread_attr_setschedparam(&attr, &schedattr)");
    status = pthread_attr_getschedparam(&attr, &schedattr);
    tryExit(status, "th_ObjectDetector__TinyDarknet - pthread_attr_getschedparam(&attr, &schedattr)");
    status = pthread_create(&th_ObjectDetector__TinyDarknet, &attr, &__ObjectDetector__TinyDarknet__, NULL);
    tryExit(status, "pthread_create(&th_ObjectDetector__TinyDarknet, &attr, &__ObjectDetector__TinyDarknet__, NULL)\n");
    // ack just created thread
    status = sem_wait(&main_ack);
    tryExit(status, "th_ObjectDetector__TinyDarknet - sem_wait(&main_ack)");
    status = pthread_attr_destroy(&attr);
    tryExit(status, "th_ObjectDetector__TinyDarknet - pthread_attr_destroy(&attr)");

    status = pthread_attr_init(&attr);
    tryExit(status, "th_SaveVideo__v1 - pthread_attr_init(&attr)");
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    tryExit(status, "th_SaveVideo__v1 - pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED)");
    status = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    tryExit(status, "th_SaveVideo__v1 - pthread_attr_setschedpolicy(&attr, SCHED_FIFO)");
    schedattr.sched_priority = 3;
    status = pthread_attr_setschedparam(&attr, &schedattr);
    tryExit(status, "th_SaveVideo__v1 - pthread_attr_setschedparam(&attr, &schedattr)");
    status = pthread_attr_getschedparam(&attr, &schedattr);
    tryExit(status, "th_SaveVideo__v1 - pthread_attr_getschedparam(&attr, &schedattr)");
    status = pthread_create(&th_SaveVideo__v1, &attr, &__SaveVideo__v1__, NULL);
    tryExit(status, "pthread_create(&th_SaveVideo__v1, &attr, &__SaveVideo__v1__, NULL)\n");
    // ack just created thread
    status = sem_wait(&main_ack);
    tryExit(status, "th_SaveVideo__v1 - sem_wait(&main_ack)");
    status = pthread_attr_destroy(&attr);
    tryExit(status, "th_SaveVideo__v1 - pthread_attr_destroy(&attr)");

    status = pthread_attr_init(&attr);
    tryExit(status, "th_Decision__v1 - pthread_attr_init(&attr)");
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    tryExit(status, "th_Decision__v1 - pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED)");
    status = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    tryExit(status, "th_Decision__v1 - pthread_attr_setschedpolicy(&attr, SCHED_FIFO)");
    schedattr.sched_priority = 3;
    status = pthread_attr_setschedparam(&attr, &schedattr);
    tryExit(status, "th_Decision__v1 - pthread_attr_setschedparam(&attr, &schedattr)");
    status = pthread_attr_getschedparam(&attr, &schedattr);
    tryExit(status, "th_Decision__v1 - pthread_attr_getschedparam(&attr, &schedattr)");
    status = pthread_create(&th_Decision__v1, &attr, &__Decision__v1__, NULL);
    tryExit(status, "pthread_create(&th_Decision__v1, &attr, &__Decision__v1__, NULL)\n");
    // ack just created thread
    status = sem_wait(&main_ack);
    tryExit(status, "th_Decision__v1 - sem_wait(&main_ack)");
    status = pthread_attr_destroy(&attr);
    tryExit(status, "th_Decision__v1 - pthread_attr_destroy(&attr)");

    status = pthread_attr_init(&attr);
    tryExit(status, "th_SendMessage__v1 - pthread_attr_init(&attr)");
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    tryExit(status, "th_SendMessage__v1 - pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED)");
    status = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    tryExit(status, "th_SendMessage__v1 - pthread_attr_setschedpolicy(&attr, SCHED_FIFO)");
    schedattr.sched_priority = 4;
    status = pthread_attr_setschedparam(&attr, &schedattr);
    tryExit(status, "th_SendMessage__v1 - pthread_attr_setschedparam(&attr, &schedattr)");
    status = pthread_attr_getschedparam(&attr, &schedattr);
    tryExit(status, "th_SendMessage__v1 - pthread_attr_getschedparam(&attr, &schedattr)");
    status = pthread_create(&th_SendMessage__v1, &attr, &__SendMessage__v1__, NULL);
    tryExit(status, "pthread_create(&th_SendMessage__v1, &attr, &__SendMessage__v1__, NULL)\n");
    // ack just created thread
    status = sem_wait(&main_ack);
    tryExit(status, "th_SendMessage__v1 - sem_wait(&main_ack)");
    status = pthread_attr_destroy(&attr);
    tryExit(status, "th_SendMessage__v1 - pthread_attr_destroy(&attr)");


    ;

    // release all tasks and sources will be waiting for the next period
    status = pthread_barrier_wait(&main_barrier);
    if(status != 0 && status != PTHREAD_BARRIER_SERIAL_THREAD)
        errExit("pthread_barrier_wait(&main_barrier)");

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = handler_timer_start_iteration;
    sigemptyset(&sa.sa_mask);
    status = sigaction(SIGRTMIN, &sa, NULL);
    tryExit(status, "sigaction");

    //set timer, as now all threads are waiting for their predecessors to complete
    te.sigev_notify = SIGEV_SIGNAL;
    te.sigev_signo = SIGRTMIN;
    te.sigev_value.sival_ptr = &timer;
    status = timer_create(CLOCK_REALTIME, &te, &timer);
    tryExit(status, "timer_create(CLOCK_REALTIME, &te, &timer)");

#if 20000000 < INT_MAX
    its.it_interval.tv_sec = 0;
    its.it_value.tv_sec = 0;
    its.it_interval.tv_nsec = 20000000;
    its.it_value.tv_nsec = 20000000 * 3;
#else
    its.it_interval.tv_sec = 20000000 / 1000000000;
    its.it_value.tv_sec = (20000000 / 1000000000) * 3;
    its.it_interval.tv_nsec = 0;
    its.it_value.tv_nsec = 0;
#endif
    status = timer_settime(timer, 0, &its, NULL);
    tryExit(status, "timer_settime(timer, 0, &its, NULL) ");

    
    status = pthread_join(th_ImageCapture__v1, NULL);
    tryExit(status, "pthread_join(th_ImageCapture__v1, NULL)");
    status = pthread_join(th_GroundSpeed__v1, NULL);
    tryExit(status, "pthread_join(th_GroundSpeed__v1, NULL)");
    status = pthread_join(th_Encryption__RSA, NULL);
    tryExit(status, "pthread_join(th_Encryption__RSA, NULL)");
    status = pthread_join(th_ObjectDetector__TinyDarknet, NULL);
    tryExit(status, "pthread_join(th_ObjectDetector__TinyDarknet, NULL)");
    status = pthread_join(th_SaveVideo__v1, NULL);
    tryExit(status, "pthread_join(th_SaveVideo__v1, NULL)");
    status = pthread_join(th_Decision__v1, NULL);
    tryExit(status, "pthread_join(th_Decision__v1, NULL)");
    status = pthread_join(th_SendMessage__v1, NULL);
    tryExit(status, "pthread_join(th_SendMessage__v1, NULL)");

    status = pthread_barrier_destroy(&main_barrier);
    tryExit(status, "pthread_barrier_destroy(&main_barrier)");

    return status;
}

