import serial
import subprocess
import time
import os

number_runs = range(100)

def send_temp(ser):
    temp=[]

    for i in range(5):
        with open('/sys/devices/virtual/thermal/thermal_zone'+str(i)+'/temp', 'r') as file:
            for line in file:
                temp.append(line)

    for each in temp:
        send = each[:-1].encode('utf-8')
        ser.write(send)
        ser.write(b',')

    ser.write(b'\n')

def warm_up(command):
    ser = serial.Serial('/dev/ttySAC0', baudrate=115200, bytesize=8, parity='N', stopbits=2, timeout=None, xonxoff=0, rtscts=0)
    ser.write(b'Temp: \n')
    for i in range(50):
        subprocess.run(command, cwd=odroid)
        send_temp(ser)
    ser.close()


odroid = "/home/odroid/ToyExample/taskgraphs/"

command = "./DAG2/ilp_generated/ToyExample"
warm_up(command)
ser = serial.Serial('/dev/ttySAC0', baudrate=115200, bytesize=8, parity='N', stopbits=2, timeout=None, xonxoff=0, rtscts=0)

for ind in number_runs:
    print(ind)
    ser.write(("start_run: " + str(ind)).encode('ascii'))
    ser.write(b'\n');

    subprocess.run(command, cwd=odroid)

    ser.write(("end").encode('ascii'))
    ser.write(b'\n')

ser.close()
