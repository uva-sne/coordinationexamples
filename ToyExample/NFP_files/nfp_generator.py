import os
import pandas
from multiprocessing import Pool
import numpy


def analyse_files(data):
    path,component,core = data
    path = os.path.join(path,component,core)
    files = os.listdir(path)

    time_taken = []
    mWh_taken = []

    for file in files:
        file_path = os.path.join(path, file)

        df = pandas.read_csv(file_path)

        df['pre_dyn'] = (df['Watt'] - 3) #hardcoded static watt
        df['pre_dyn_sum'] = df['pre_dyn'].rolling(2).sum()
        df['pre_dyn_sum'] = df['pre_dyn_sum']/2*0.00025 #0.00025seconds between measurements
        joule = df['pre_dyn_sum'].sum()

        time = df['Time Stamp'].tail(1).item() - df['Time Stamp'].head(1).item()

        mWh_taken.append(joule)
        time_taken.append(time)


    # print(component, core, [numpy.average(time_taken), numpy.average(mWh_taken), numpy.max(time_taken), numpy.max(mWh_taken)])
    print(component, core, [numpy.mean(time_taken)*1.2, numpy.mean(mWh_taken)*1])

if __name__ == '__main__':
    path = "/home/julius/HDD/SURF/EnergyData/ToyExample/"
    # file = "big_core_MatrixMult_conv.csv"
    components = ['MatrixMult', 'MatrixMultGPU', 'Convolution', 'Convolution_gpu', 'MatrixMultOpt', 'ConvolutionOpt', 'AddMatrix', 'AddMatrixGPU',  'AddMatrixOpt', 'SubMatrix', 'SubMatrixGPU', 'SubMatrixOpt', 'DivisionMatrix', 'DivisionMatrixGPU', 'DivisionMatrixOpt', 'MultMatrix', 'MultMatrixGPU', 'MultMatrixOpt']
    # components = ['DivisionMatrixOpt']

    core = "little"

    # components = ['First','Add','Sub','Mult','Division','Mult_double','Add_double','Div_double','Sub_double','Sum_all']



    data = []
    for component in components:
        data.append([path, component, core])

    with Pool(1) as pool:
        pool.map(analyse_files, data)
