import os
import pandas
from multiprocessing import Pool

def ensure_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def preprocessing(path, file, components, core):
    start_run = 0
    benchmark_data = []
    current_component = ''
    current_run_done = 0
    component_count = dict(zip(components, [0]*len(components)))

    file_path = os.path.join(path, file)

    with open(file_path,"r") as raw_data:
        for line in raw_data:
            data = line.split(",")
            if len(data) > 3:
                if data[4] != "\n":
                    for component in components:
                        if component in data[4]:
                            current_component = component
                    if "Start" in data[4]:
                        start_run = 1
                    elif "Stop" in data[4]:
                        benchmark_data.append(line)
                        start_run = 0
                        current_run_done = 1

            if start_run:
                benchmark_data.append(line)

            if current_run_done:
                new_path = os.path.join(path,current_component,core)
                ensure_dir(new_path)
                new_file = str(component_count[current_component])+'.csv'
                new_file_path = os.path.join(new_path, new_file)
                print(new_file_path)
                with open(new_file_path, "w") as test_file:
                    for run_line in benchmark_data:
                        test_file.writelines(run_line)
                component_count[current_component] += 1
                start_run = 0
                current_run_done = 0
                current_component = ''
                benchmark_data = []


def remove_empty_lines(stuff):
    path,component,core = stuff
    path = os.path.join(path,component,core)
    files = os.listdir(path)

    for file in files:
        file_path = os.path.join(path, file)
        dirty_file_path = os.path.join(path, file[:-4] + "_dirty.csv")

        with open(file_path, "r") as benchmark_data:
            lines = benchmark_data.readlines()

        os.rename(file_path, dirty_file_path)

        with open(file_path, "x") as f:
            for line in lines:
                if line != '\n':
                    f.write(line)

        os.remove(dirty_file_path)

    print("Done with: " + component)


def parse_line(line):
    """parse lines from text CSV to """
    data_list = line.split(',')

    data_list[0] = float(data_list[0])  # extract time stamp
    data_list[1] = float(data_list[1])  # extract amp
    if data_list[2] != "":
        data_list[2] = float(data_list[2])  # extract voltage if exists
    else:
        data_list[2] = 0
    data_list[3] = float(data_list[3])  # extract mWh
    data_list.pop(4)

    return data_list


def add_line_df(line, list_dicts):
    columns = ['Time Stamp', 'Amp', 'Voltage', 'J']
    parsed_line = parse_line(line)
    list_dicts.append(dict(zip(columns, parsed_line)))


def add_original_voltage(df):
    df['original voltage'] = df['Voltage'].gt(0)
    return df


def calculate_watt(df):
    df['Watt'] = df['Voltage'] * df['Amp']
    return df


def fill_in_voltage(df):
    new_voltage = []
    for v in df['Voltage'].items():
        if v[1] > 0:
            current_voltage = v[1]
        new_voltage.append(current_voltage)

    df['Voltage'] = new_voltage
    return df


def convert_files_in_benchmark(stuff):
    path,component,core = stuff
    path = os.path.join(path,component,core)
    files = os.listdir(path)
    for file in files:
        file_path = os.path.join(path, file)
        list_dicts = []

        if not os.path.isdir(file_path):
            if "csv" in file_path:
                with open(file_path, "rt") as stream:
                    for line in stream:
                        add_line_df(line, list_dicts)

                df = pandas.DataFrame.from_dict(list_dicts)
                df = add_original_voltage(df)
                df = fill_in_voltage(df)
                df = calculate_watt(df)

                file = file.replace(".csv","_df.csv.gz")
                new_file_path = os.path.join(path, file)
                # print(new_file_path)
                df.to_csv(path_or_buf=new_file_path)
                os.remove(file_path)
    print(df)
    print("Done")


if __name__ == '__main__':

    path = "/home/julius/HDD/SURF/EnergyData/ToyExample/"
    file = "Matrix_rest_little.csv"
    core = "little"
    # components = ['First','Add','Sub','Mult','Division','Mult_double','Add_double','Div_double','Sub_double','Sum_all']
    # components = ['MatrixMult', 'MatrixMultGPU', 'Convolution', 'Convolution_gpu']
    components = ['MatrixMultOpt', 'ConvolutionOpt', 'AddMatrix', 'AddMatrixGPU',  'AddMatrixOpt', 'SubMatrix', 'SubMatrixGPU', 'SubMatrixOpt', 'DivisionMatrix', 'DivisionMatrixGPU', 'DivisionMatrixOpt', 'MultMatrix', 'MultMatrixGPU', 'MultMatrixOpt']

    preprocessing(path, file, components, core)

    data = []
    for component in components:
        data.append([path, component, core])

    with Pool(8) as pool:
        pool.map(remove_empty_lines, data)

    with Pool(8) as pool:
        pool.map(convert_files_in_benchmark, data)
