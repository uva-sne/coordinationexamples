#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <sched.h>

#include "ToyExample.h"

#define NUM_CORES 8
#define MAX_NSECOND 1000000000
#define RUNTIME_FOR_WAIT 1000

#define errExit(msg) { perror(msg); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) {printf("-> %d <-\n",var); errExit(msg) }
#define STRINGIFY(X) #X
#define CONCAT(W,X,Y) W ## X ## Y

#define THREAD_INSTANTIATE(PROC, TASK, PRIORITY, MAPPING) status = pthread_attr_init(&attr); \
    tryExit(status, STRINGIFY(TASK) " - pthread_attr_init(&attr)" ); \
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);\
    tryExit(status, STRINGIFY(TASK) " pthread_attr_setinheritsched " );\
    status = pthread_attr_setschedpolicy (&attr, SCHED_FIFO);\
    tryExit(status, STRINGIFY(TASK) " pthread_attr_setschedpolicy " );\
    if(PRIORITY > 0) {\
        struct sched_param param;\
        param.sched_priority = PRIORITY;\
        status = pthread_attr_setschedparam(&attr, &param); \
        tryExit(status, STRINGIFY(TASK) " - pthread_attr_setschedprio(&attr,&param)"); \
    }\
    if(MAPPING >= 0) {\
        cpu_set_t cpuset;\
        CPU_ZERO(&cpuset);\
        CPU_SET(MAPPING, &cpuset);\
        status = pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpuset); \
        tryExit(status, STRINGIFY(TASK) " - pthread_attr_setaffinity_np(&attr, " STRINGIFY(MAPPING) ")" );\
    }\
    status = pthread_create(PROC, &attr, TASK, NULL); \
    tryExit(status, "pthread_create( " STRINGIFY(PROC) " , &attr, " STRINGIFY(TASK) " , NULL)\n"); \
    status = pthread_attr_destroy(&attr); \
    tryExit(status, STRINGIFY(TASK) " - pthread_attr_destroy(&attr)");

int first_out=0;
double division_out=0;
double add_out=0;
double sub_out=0;
double mult_out=0;

double add_double_out=0;
double sub_double_out=0;
double mult_double_out=0;
double division_double_out=0;

int true_sleep(int seconds, struct timespec start){ //TODO: MEASURE TIME FOR THIS FUNCTION
    struct timespec end;
    end = start;
    end.tv_sec += seconds;
    // end.tv_nsec -= NANOSECONDS_TIMING;

    if(clock_nanosleep(CLOCK_MONOTONIC, 1, &end, NULL)){
        printf("shit missed deadline\n");
            return 1;
    }

    return 0;
}

void *core0(void* ){
}

void *core1(void* ){
}

void *core2(void* ){
}

void *core3(void* ){
}

void *core4(void* ){
    struct timespec start; //TODO: MEASURE TIME FOR THESE TWO INSTRUCTIONS
    clock_gettime(CLOCK_MONOTONIC, &start);
    true_sleep(1, start); //sleep one second for the first component to be done
    clock_gettime(CLOCK_MONOTONIC, &start);
    mult(first_out,&mult_out); //T=12

    true_sleep(5, start); //
    division_double(first_out,&division_double_out);
}

void *core5(void* ){
    struct timespec start; //TODO: MEASURE TIME FOR THESE TWO INSTRUCTIONS
    clock_gettime(CLOCK_MONOTONIC, &start);
    true_sleep(1, start); //sleep one second for the first component to be done
    clock_gettime(CLOCK_MONOTONIC, &start);
    add_double(first_out,&add_out); //T=12

    true_sleep(7, start); //
    sub_double(first_out,&sub_double_out);
}

void *core6(void* ){
    struct timespec start; //TODO: MEASURE TIME FOR THESE TWO INSTRUCTIONS
    clock_gettime(CLOCK_MONOTONIC, &start);
    true_sleep(1, start); //sleep one second for the first component to be done
    clock_gettime(CLOCK_MONOTONIC, &start);
    division(first_out,&division_out); //T=12

    true_sleep(8, start); //
    sub(first_out,&sub_out);
}

void *core7(void* ){
    //start time
    struct timespec start; //TODO: MEASURE TIME FOR THESE TWO INSTRUCTIONS
    clock_gettime(CLOCK_MONOTONIC, &start);
    first(&first_out); //T=0
    // first_duplicate();

    //wait 1 second
    true_sleep(1, start);
    clock_gettime(CLOCK_MONOTONIC, &start);
    mult_double(first_out,&mult_double_out);//T=11

    //wait 1 second
    true_sleep(8, start);
    clock_gettime(CLOCK_MONOTONIC, &start);
    add(first_out,&add_double_out);//T=11

    //wait an additional 11 seconds
    true_sleep(6, start);
    clock_gettime(CLOCK_MONOTONIC, &start);
    sum_all(division_out, add_out, sub_out, mult_out, add_double_out, sub_double_out, mult_double_out, division_double_out);//T=22
}


int main(void) {
    int status = 0;
    pthread_attr_t attr;

    pthread_t cores[NUM_CORES];
    int input[NUM_CORES];
    void *results[NUM_CORES];
    size_t i;

    for (i = 0; i < NUM_CORES; i++) {
        input[i] = i;
    }

    THREAD_INSTANTIATE(&cores[7], &core7, 10, 7);
    THREAD_INSTANTIATE(&cores[0], &core0, 10, 0);
    THREAD_INSTANTIATE(&cores[1], &core1, 10, 1);
    THREAD_INSTANTIATE(&cores[2], &core2, 10, 2);
    THREAD_INSTANTIATE(&cores[3], &core3, 10, 3);
    THREAD_INSTANTIATE(&cores[4], &core4, 10, 4);
    THREAD_INSTANTIATE(&cores[5], &core5, 10, 5);
    THREAD_INSTANTIATE(&cores[6], &core6, 10, 6);

    for (i = 0; i < NUM_CORES; i++) {
        pthread_join(cores[i], &results[i]);
    }
    return 0;
}
