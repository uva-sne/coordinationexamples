
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <semaphore.h> 
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <sched.h>

#include "ToyExample.h"
#include "odroid_uart.h"
#include "shared_defs.h"


static inline int sem_wait_nointr(sem_t *sem) {
    while (sem_wait(sem)) {
        if (errno == EINTR) errno = 0;
        else return -1;
    }
    return 0;
}

void main_clean();

#define errExit(msg) { perror(msg); main_clean(); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) {printf("-> %d <-\n",var); errno = var ; errExit(msg) }
#define STRINGIFY(X) #X
#define CONCAT(W,X,Y,Z) W ## X ## Y ## Z

#define THREAD_INSTANTIATE(TASK, VERSI, SHORTNAME, PRIORITY, MAPPING) status = pthread_attr_init(&attr); \
    tryExit(status, STRINGIFY(TASK) " - pthread_attr_init(&attr)"); \
    status = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);\
    tryExit(status, STRINGIFY(TASK) " pthread_attr_setinheritsched ");\
    status = pthread_attr_setschedpolicy (&attr, SCHED_FIFO);\
    tryExit(status, STRINGIFY(TASK) " pthread_attr_setschedpolicy ");\
    if(PRIORITY > 0) {\
        struct sched_param param;\
        param.sched_priority = PRIORITY;\
        status = pthread_attr_setschedparam(&attr, &param); \
        tryExit(status, STRINGIFY(TASK) " - pthread_attr_setschedprio(&attr,&param)"); \
    }\
    if(MAPPING >= 0) {\
        cpu_set_t cpuset;\
        CPU_ZERO(&cpuset);\
        CPU_SET(MAPPING, &cpuset);\
        status = pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpuset); \
        tryExit(status, STRINGIFY(TASK) " - pthread_attr_setaffinity_np(&attr," STRINGIFY(MAPPING) ")");\
    }\
    status = pthread_create(CONCAT(&th_,TASK,_,VERSI), &attr, CONCAT(&__,TASK,_,VERSI), NULL); \
    tryExit(status, "pthread_create(&th_" STRINGIFY(TASK) "_" STRINGIFY(VERSI) ", &attr, &" STRINGIFY(TASK) "_" STRINGIFY(VERSI) ", NULL)\n"); \
    status = pthread_setname_np(CONCAT(th_,TASK,_,VERSI), STRINGIFY(SHORTNAME));\
    tryExit(status, STRINGIFY(TASK) " - pthread_setname_np(" STRINGIFY(TASK) ", " STRINGIFY(SHORTNAME) ")");\
    status = pthread_attr_destroy(&attr); \
    tryExit(status, STRINGIFY(TASK) " - pthread_attr_destroy(&attr)");


sem_t *__start_lock_ = NULL;
timer_t timer_;


buffer_int1_t first_a;
buffer_int1_t *add_a;
buffer_double1_t add_b;
buffer_int1_t *sub_a;
buffer_double1_t sub_c;
buffer_int1_t *mult_a;
buffer_double1_t mult_d;
buffer_int1_t *division_a;
buffer_double1_t division_e;
buffer_double1_t *add_double_c;
buffer_double1_t add_double_f;
buffer_double1_t *sub_double_e;
buffer_double1_t sub_double_g;
buffer_double1_t *mult_double_b;
buffer_double1_t mult_double_h;
buffer_double1_t *division_double_d;
buffer_double1_t division_double_i;
buffer_double1_t *sum_all_f;
buffer_double1_t *sum_all_g;
buffer_double1_t *sum_all_h;
buffer_double1_t *sum_all_i;
buffer_int1_t *__dupintern__first_a_in;
buffer_int1_t __dupintern__first_a_out4;
buffer_int1_t __dupintern__first_a_out3;
buffer_int1_t __dupintern__first_a_out2;
buffer_int1_t __dupintern__first_a_out1;


sem_t *first_lock = NULL;
sem_t *add_lock = NULL;
sem_t *sub_lock = NULL;
sem_t *mult_lock = NULL;
sem_t *division_lock = NULL;
sem_t *add_double_lock = NULL;
sem_t *sub_double_lock = NULL;
sem_t *mult_double_lock = NULL;
sem_t *division_double_lock = NULL;
sem_t *__dupintern__first_a_lock = NULL;


pthread_t th_first_v1a;
pthread_t th_first_v1b;
pthread_t th___dupintern__first_a_;
pthread_t th_division_v1a;
pthread_t th_division_v1b;
pthread_t th_mult_v1a;
pthread_t th_mult_v1b;
pthread_t th_add_v1a;
pthread_t th_add_v1b;
pthread_t th_sub_v1a;
pthread_t th_sub_v1b;
pthread_t th_sub_double_v1a;
pthread_t th_sub_double_v1b;
pthread_t th_division_double_v1a;
pthread_t th_division_double_v1b;
pthread_t th_mult_double_v1a;
pthread_t th_mult_double_v1b;
pthread_t th_add_double_v1a;
pthread_t th_add_double_v1b;
pthread_t th_sum_all_v1a;
pthread_t th_sum_all_v1b;



void __coord_first_v1a() {
    int status;

    

    int a = 0;
int  __bkinit__a = a;

    first(&a);
    
if(a != __bkinit__a)
    push_int1(&first_a, a);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(first_lock);
#endif
}

void __coord_first_v1b() {
    int status;

    

    int a = 0;
int  __bkinit__a = a;

    first(&a);
    
if(a != __bkinit__a)
    push_int1(&first_a, a);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(first_lock);
#endif
}

void __coord_add_v1a() {
    int status;

    
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }


    int a = 0;
double b = 0;
double  __bkinit__b = b;
pop_int1(add_a, &a);

    add(a, &b);
    
if(b != __bkinit__b)
    push_double1(&add_b, b);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(add_lock);
#endif
}

void __coord_add_v1b() {
    int status;

    
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }


    int a = 0;
double b = 0;
double  __bkinit__b = b;
pop_int1(add_a, &a);

    add(a, &b);
    
if(b != __bkinit__b)
    push_double1(&add_b, b);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(add_lock);
#endif
}

void __coord_sub_v1a() {
    int status;

    
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }


    int a = 0;
double c = 0;
double  __bkinit__c = c;
pop_int1(sub_a, &a);

    sub(a, &c);
    
if(c != __bkinit__c)
    push_double1(&sub_c, c);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(sub_lock);
#endif
}

void __coord_sub_v1b() {
    int status;

    
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }


    int a = 0;
double c = 0;
double  __bkinit__c = c;
pop_int1(sub_a, &a);

    sub(a, &c);
    
if(c != __bkinit__c)
    push_double1(&sub_c, c);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(sub_lock);
#endif
}

void __coord_mult_v1a() {
    int status;

    
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }


    int a = 0;
double d = 0;
double  __bkinit__d = d;
pop_int1(mult_a, &a);

    mult(a, &d);
    
if(d != __bkinit__d)
    push_double1(&mult_d, d);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(mult_lock);
#endif
}

void __coord_mult_v1b() {
    int status;

    
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }


    int a = 0;
double d = 0;
double  __bkinit__d = d;
pop_int1(mult_a, &a);

    mult(a, &d);
    
if(d != __bkinit__d)
    push_double1(&mult_d, d);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(mult_lock);
#endif
}

void __coord_division_v1a() {
    int status;

    
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }


    int a = 0;
double e = 0;
double  __bkinit__e = e;
pop_int1(division_a, &a);

    division(a, &e);
    
if(e != __bkinit__e)
    push_double1(&division_e, e);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(division_lock);
#endif
}

void __coord_division_v1b() {
    int status;

    
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }


    int a = 0;
double e = 0;
double  __bkinit__e = e;
pop_int1(division_a, &a);

    division(a, &e);
    
if(e != __bkinit__e)
    push_double1(&division_e, e);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(division_lock);
#endif
}

void __coord_add_double_v1a() {
    int status;

    
    status = sem_wait_nointr(sub_lock);
    if(status != 0) {
        perror("sem_wait_nointr(sub_lock)");
        return;
    }


    double c = 0;
double f = 0;
double  __bkinit__f = f;
pop_double1(add_double_c, &c);

    add_double(c, &f);
    
if(f != __bkinit__f)
    push_double1(&add_double_f, f);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(add_double_lock);
#endif
}

void __coord_add_double_v1b() {
    int status;

    
    status = sem_wait_nointr(sub_lock);
    if(status != 0) {
        perror("sem_wait_nointr(sub_lock)");
        return;
    }


    double c = 0;
double f = 0;
double  __bkinit__f = f;
pop_double1(add_double_c, &c);

    add_double(c, &f);
    
if(f != __bkinit__f)
    push_double1(&add_double_f, f);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(add_double_lock);
#endif
}

void __coord_sub_double_v1a() {
    int status;

    
    status = sem_wait_nointr(division_lock);
    if(status != 0) {
        perror("sem_wait_nointr(division_lock)");
        return;
    }


    double e = 0;
double g = 0;
double  __bkinit__g = g;
pop_double1(sub_double_e, &e);

    sub_double(e, &g);
    
if(g != __bkinit__g)
    push_double1(&sub_double_g, g);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(sub_double_lock);
#endif
}

void __coord_sub_double_v1b() {
    int status;

    
    status = sem_wait_nointr(division_lock);
    if(status != 0) {
        perror("sem_wait_nointr(division_lock)");
        return;
    }


    double e = 0;
double g = 0;
double  __bkinit__g = g;
pop_double1(sub_double_e, &e);

    sub_double(e, &g);
    
if(g != __bkinit__g)
    push_double1(&sub_double_g, g);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(sub_double_lock);
#endif
}

void __coord_mult_double_v1a() {
    int status;

    
    status = sem_wait_nointr(add_lock);
    if(status != 0) {
        perror("sem_wait_nointr(add_lock)");
        return;
    }


    double b = 0;
double h = 0;
double  __bkinit__h = h;
pop_double1(mult_double_b, &b);

    mult_double(b, &h);
    
if(h != __bkinit__h)
    push_double1(&mult_double_h, h);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(mult_double_lock);
#endif
}

void __coord_mult_double_v1b() {
    int status;

    
    status = sem_wait_nointr(add_lock);
    if(status != 0) {
        perror("sem_wait_nointr(add_lock)");
        return;
    }


    double b = 0;
double h = 0;
double  __bkinit__h = h;
pop_double1(mult_double_b, &b);

    mult_double(b, &h);
    
if(h != __bkinit__h)
    push_double1(&mult_double_h, h);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(mult_double_lock);
#endif
}

void __coord_division_double_v1a() {
    int status;

    
    status = sem_wait_nointr(mult_lock);
    if(status != 0) {
        perror("sem_wait_nointr(mult_lock)");
        return;
    }


    double d = 0;
double i = 0;
double  __bkinit__i = i;
pop_double1(division_double_d, &d);

    division_double(d, &i);
    
if(i != __bkinit__i)
    push_double1(&division_double_i, i);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(division_double_lock);
#endif
}

void __coord_division_double_v1b() {
    int status;

    
    status = sem_wait_nointr(mult_lock);
    if(status != 0) {
        perror("sem_wait_nointr(mult_lock)");
        return;
    }


    double d = 0;
double i = 0;
double  __bkinit__i = i;
pop_double1(division_double_d, &d);

    division_double(d, &i);
    
if(i != __bkinit__i)
    push_double1(&division_double_i, i);


#if 1 > 0
    for(int __i__=0 ; __i__ < 1 ; ++__i__)
        sem_post(division_double_lock);
#endif
}

void __coord_sum_all_v1a() {
    int status;

    
    status = sem_wait_nointr(add_double_lock);
    if(status != 0) {
        perror("sem_wait_nointr(add_double_lock)");
        return;
    }

    status = sem_wait_nointr(sub_double_lock);
    if(status != 0) {
        perror("sem_wait_nointr(sub_double_lock)");
        return;
    }

    status = sem_wait_nointr(mult_double_lock);
    if(status != 0) {
        perror("sem_wait_nointr(mult_double_lock)");
        return;
    }

    status = sem_wait_nointr(division_double_lock);
    if(status != 0) {
        perror("sem_wait_nointr(division_double_lock)");
        return;
    }


    double f = 0;
double g = 0;
double h = 0;
double i = 0;
pop_double1(sum_all_f, &f);
pop_double1(sum_all_g, &g);
pop_double1(sum_all_h, &h);
pop_double1(sum_all_i, &i);

    sum_all(f, g, h, i);
    

#if 0 > 0
    for(int __i__=0 ; __i__ < 0 ; ++__i__)
        sem_post(sum_all_lock);
#endif
}

void __coord_sum_all_v1b() {
    int status;

    
    status = sem_wait_nointr(add_double_lock);
    if(status != 0) {
        perror("sem_wait_nointr(add_double_lock)");
        return;
    }

    status = sem_wait_nointr(sub_double_lock);
    if(status != 0) {
        perror("sem_wait_nointr(sub_double_lock)");
        return;
    }

    status = sem_wait_nointr(mult_double_lock);
    if(status != 0) {
        perror("sem_wait_nointr(mult_double_lock)");
        return;
    }

    status = sem_wait_nointr(division_double_lock);
    if(status != 0) {
        perror("sem_wait_nointr(division_double_lock)");
        return;
    }


    double f = 0;
double g = 0;
double h = 0;
double i = 0;
pop_double1(sum_all_f, &f);
pop_double1(sum_all_g, &g);
pop_double1(sum_all_h, &h);
pop_double1(sum_all_i, &i);

    sum_all(f, g, h, i);
    

#if 0 > 0
    for(int __i__=0 ; __i__ < 0 ; ++__i__)
        sem_post(sum_all_lock);
#endif
}

void __coord___dupintern__first_a_() {
    int status;

    
    status = sem_wait_nointr(first_lock);
    if(status != 0) {
        perror("sem_wait_nointr(first_lock)");
        return;
    }


    int in = 0;
int out4 = 0;
int  __bkinit__out4 = out4;
int out3 = 0;
int  __bkinit__out3 = out3;
int out2 = 0;
int  __bkinit__out2 = out2;
int out1 = 0;
int  __bkinit__out1 = out1;
pop_int1(__dupintern__first_a_in, &in);

    out4 = in;
out3 = in;
out2 = in;
out1 = in;

    
if(out4 != __bkinit__out4)
    push_int1(&__dupintern__first_a_out4, out4);

if(out3 != __bkinit__out3)
    push_int1(&__dupintern__first_a_out3, out3);

if(out2 != __bkinit__out2)
    push_int1(&__dupintern__first_a_out2, out2);

if(out1 != __bkinit__out1)
    push_int1(&__dupintern__first_a_out1, out1);


#if 4 > 0
    for(int __i__=0 ; __i__ < 4 ; ++__i__)
        sem_post(__dupintern__first_a_lock);
#endif
}



void * __first_v1a(void* unused)
{
    int status;

    

    for(;;) {
        
    status = sem_wait_nointr(__start_lock_);
    if(status != 0) {
        perror("sem_wait_nointr(__start_lock_)");
        //return;
    }


        __coord_first_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __first_v1b(void* unused)
{
    int status;

    

    for(;;) {
        
    status = sem_wait_nointr(__start_lock_);
    if(status != 0) {
        perror("sem_wait_nointr(__start_lock_)");
        //return;
    }


        __coord_first_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * ____dupintern__first_a_(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord___dupintern__first_a_();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __division_v1a(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_division_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __division_v1b(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_division_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __mult_v1a(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_mult_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __mult_v1b(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_mult_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __add_v1a(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_add_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __add_v1b(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_add_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __sub_v1a(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_sub_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __sub_v1b(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_sub_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __sub_double_v1a(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_sub_double_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __sub_double_v1b(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_sub_double_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __division_double_v1a(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_division_double_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __division_double_v1b(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_division_double_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __mult_double_v1a(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_mult_double_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __mult_double_v1b(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_mult_double_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __add_double_v1a(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_add_double_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __add_double_v1b(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_add_double_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __sum_all_v1a(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_sum_all_v1a();

        
        
        sched_yield();
    }

    

    return NULL;
}



void * __sum_all_v1b(void* unused)
{
    int status;

    

    for(;;) {
        

        __coord_sum_all_v1b();

        
        
        sched_yield();
    }

    

    return NULL;
}



static void handler_timer_(int sig, siginfo_t *si, void *uc) {
    for(size_t i=0 ; i < 1 ; ++i)
        sem_post(__start_lock_);
}


int main_init() {
    int status = 0;

    	init_buffer_int1(&first_a);
	init_buffer_double1(&add_b);
	init_buffer_double1(&sub_c);
	init_buffer_double1(&mult_d);
	init_buffer_double1(&division_e);
	init_buffer_double1(&add_double_f);
	init_buffer_double1(&sub_double_g);
	init_buffer_double1(&mult_double_h);
	init_buffer_double1(&division_double_i);
	init_buffer_int1(&__dupintern__first_a_out4);
	init_buffer_int1(&__dupintern__first_a_out3);
	init_buffer_int1(&__dupintern__first_a_out2);
	init_buffer_int1(&__dupintern__first_a_out1);
	add_a = &__dupintern__first_a_out1;
	sub_a = &__dupintern__first_a_out2;
	mult_a = &__dupintern__first_a_out3;
	division_a = &__dupintern__first_a_out4;
	add_double_c = &sub_c;
	sub_double_e = &division_e;
	mult_double_b = &add_b;
	division_double_d = &mult_d;
	sum_all_f = &add_double_f;
	sum_all_g = &sub_double_g;
	sum_all_h = &mult_double_h;
	sum_all_i = &division_double_i;
	__dupintern__first_a_in = &first_a;


    
#if 0 == 1
first_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (first_lock == NULL);
tryExit(status, "mmap first_lock");
status = mlock(first_lock, sizeof(sem_t));
tryExit(status, "mlock first_lock");
#else
first_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(first_lock, 0, 0);
if(status != 0) {
    perror("sem_init(first_lock, 0, 0)");
    return status;
}

#if 0 == 1
add_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (add_lock == NULL);
tryExit(status, "mmap add_lock");
status = mlock(add_lock, sizeof(sem_t));
tryExit(status, "mlock add_lock");
#else
add_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(add_lock, 0, 0);
if(status != 0) {
    perror("sem_init(add_lock, 0, 0)");
    return status;
}

#if 0 == 1
sub_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (sub_lock == NULL);
tryExit(status, "mmap sub_lock");
status = mlock(sub_lock, sizeof(sem_t));
tryExit(status, "mlock sub_lock");
#else
sub_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(sub_lock, 0, 0);
if(status != 0) {
    perror("sem_init(sub_lock, 0, 0)");
    return status;
}

#if 0 == 1
mult_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (mult_lock == NULL);
tryExit(status, "mmap mult_lock");
status = mlock(mult_lock, sizeof(sem_t));
tryExit(status, "mlock mult_lock");
#else
mult_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(mult_lock, 0, 0);
if(status != 0) {
    perror("sem_init(mult_lock, 0, 0)");
    return status;
}

#if 0 == 1
division_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (division_lock == NULL);
tryExit(status, "mmap division_lock");
status = mlock(division_lock, sizeof(sem_t));
tryExit(status, "mlock division_lock");
#else
division_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(division_lock, 0, 0);
if(status != 0) {
    perror("sem_init(division_lock, 0, 0)");
    return status;
}

#if 0 == 1
add_double_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (add_double_lock == NULL);
tryExit(status, "mmap add_double_lock");
status = mlock(add_double_lock, sizeof(sem_t));
tryExit(status, "mlock add_double_lock");
#else
add_double_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(add_double_lock, 0, 0);
if(status != 0) {
    perror("sem_init(add_double_lock, 0, 0)");
    return status;
}

#if 0 == 1
sub_double_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (sub_double_lock == NULL);
tryExit(status, "mmap sub_double_lock");
status = mlock(sub_double_lock, sizeof(sem_t));
tryExit(status, "mlock sub_double_lock");
#else
sub_double_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(sub_double_lock, 0, 0);
if(status != 0) {
    perror("sem_init(sub_double_lock, 0, 0)");
    return status;
}

#if 0 == 1
mult_double_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (mult_double_lock == NULL);
tryExit(status, "mmap mult_double_lock");
status = mlock(mult_double_lock, sizeof(sem_t));
tryExit(status, "mlock mult_double_lock");
#else
mult_double_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(mult_double_lock, 0, 0);
if(status != 0) {
    perror("sem_init(mult_double_lock, 0, 0)");
    return status;
}

#if 0 == 1
division_double_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (division_double_lock == NULL);
tryExit(status, "mmap division_double_lock");
status = mlock(division_double_lock, sizeof(sem_t));
tryExit(status, "mlock division_double_lock");
#else
division_double_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(division_double_lock, 0, 0);
if(status != 0) {
    perror("sem_init(division_double_lock, 0, 0)");
    return status;
}

#if 0 == 1
__dupintern__first_a_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (__dupintern__first_a_lock == NULL);
tryExit(status, "mmap __dupintern__first_a_lock");
status = mlock(__dupintern__first_a_lock, sizeof(sem_t));
tryExit(status, "mlock __dupintern__first_a_lock");
#else
__dupintern__first_a_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(__dupintern__first_a_lock, 0, 0);
if(status != 0) {
    perror("sem_init(__dupintern__first_a_lock, 0, 0)");
    return status;
}

#if 0 == 1
__dupintern__first_a_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (__dupintern__first_a_lock == NULL);
tryExit(status, "mmap __dupintern__first_a_lock");
status = mlock(__dupintern__first_a_lock, sizeof(sem_t));
tryExit(status, "mlock __dupintern__first_a_lock");
#else
__dupintern__first_a_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(__dupintern__first_a_lock, 0, 0);
if(status != 0) {
    perror("sem_init(__dupintern__first_a_lock, 0, 0)");
    return status;
}

#if 0 == 1
__dupintern__first_a_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (__dupintern__first_a_lock == NULL);
tryExit(status, "mmap __dupintern__first_a_lock");
status = mlock(__dupintern__first_a_lock, sizeof(sem_t));
tryExit(status, "mlock __dupintern__first_a_lock");
#else
__dupintern__first_a_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(__dupintern__first_a_lock, 0, 0);
if(status != 0) {
    perror("sem_init(__dupintern__first_a_lock, 0, 0)");
    return status;
}

#if 0 == 1
__dupintern__first_a_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (__dupintern__first_a_lock == NULL);
tryExit(status, "mmap __dupintern__first_a_lock");
status = mlock(__dupintern__first_a_lock, sizeof(sem_t));
tryExit(status, "mlock __dupintern__first_a_lock");
#else
__dupintern__first_a_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(__dupintern__first_a_lock, 0, 0);
if(status != 0) {
    perror("sem_init(__dupintern__first_a_lock, 0, 0)");
    return status;
}


    

    
#if 0 == 1
    __start_lock_ = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (__start_lock_ == NULL);
    tryExit(status, "mmap __start_lock_");
    status = mlock(__start_lock_, sizeof(sem_t));
    tryExit(status, "mlock __start_lock_");
#else
    __start_lock_ = (sem_t*)malloc(sizeof(sem_t));
#endif
    status = sem_init(__start_lock_, 0, 0);
    tryExit(status, "sem_init(__start_lock_, 0, 0)");

    struct sigaction sa;
    struct sigevent te;
    struct itimerspec its;

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = handler_timer_;
    sigemptyset(&sa.sa_mask);
    status = sigaction(SIGRTMIN+1, &sa, NULL);
    tryExit(status, "sigaction");

    //set timer, as now all threads are waiting for their predecessors to complete
    te.sigev_notify = SIGEV_SIGNAL;
    te.sigev_signo = SIGRTMIN+1;
    te.sigev_value.sival_ptr = &timer_;
    status = timer_create(CLOCK_REALTIME, &te, &timer_);
    tryExit(status, "timer_create(CLOCK_REALTIME, &te, &timer_)");

    #if 10000000 < 1000000000
    its.it_interval.tv_sec = 0;
    its.it_value.tv_sec = 0;
    its.it_interval.tv_nsec = 10000000;
    its.it_value.tv_nsec = 10000000 ;//* ;
    #else
    its.it_interval.tv_sec = 10000000 / 1000000000;
    its.it_value.tv_sec = (10000000 / 1000000000) ;//* ;
    its.it_interval.tv_nsec = 0;
    its.it_value.tv_nsec = 0;
    #endif
    status = timer_settime(timer_, 0, &its, NULL);
    tryExit(status, "timer_settime(timer_, 0, &its, NULL) ");


    return status;
}

void main_clean() {
    
timer_delete(timer_);
if(__start_lock_ != NULL) {
    sem_destroy(__start_lock_);
#if 0 == 1
    munlock(__start_lock_, sizeof(sem_t));
    munmap(__start_lock_, sizeof(sem_t));
#else
    free(__start_lock_);
#endif
    __start_lock_ = NULL;
}

    

    
if(first_lock != NULL) {
    sem_destroy(first_lock);
#if 0 == 1
    munlock(first_lock, sizeof(sem_t));
    munmap(first_lock, sizeof(sem_t));
#else
    free(first_lock);
#endif
    first_lock = NULL;
}
if(add_lock != NULL) {
    sem_destroy(add_lock);
#if 0 == 1
    munlock(add_lock, sizeof(sem_t));
    munmap(add_lock, sizeof(sem_t));
#else
    free(add_lock);
#endif
    add_lock = NULL;
}
if(sub_lock != NULL) {
    sem_destroy(sub_lock);
#if 0 == 1
    munlock(sub_lock, sizeof(sem_t));
    munmap(sub_lock, sizeof(sem_t));
#else
    free(sub_lock);
#endif
    sub_lock = NULL;
}
if(mult_lock != NULL) {
    sem_destroy(mult_lock);
#if 0 == 1
    munlock(mult_lock, sizeof(sem_t));
    munmap(mult_lock, sizeof(sem_t));
#else
    free(mult_lock);
#endif
    mult_lock = NULL;
}
if(division_lock != NULL) {
    sem_destroy(division_lock);
#if 0 == 1
    munlock(division_lock, sizeof(sem_t));
    munmap(division_lock, sizeof(sem_t));
#else
    free(division_lock);
#endif
    division_lock = NULL;
}
if(add_double_lock != NULL) {
    sem_destroy(add_double_lock);
#if 0 == 1
    munlock(add_double_lock, sizeof(sem_t));
    munmap(add_double_lock, sizeof(sem_t));
#else
    free(add_double_lock);
#endif
    add_double_lock = NULL;
}
if(sub_double_lock != NULL) {
    sem_destroy(sub_double_lock);
#if 0 == 1
    munlock(sub_double_lock, sizeof(sem_t));
    munmap(sub_double_lock, sizeof(sem_t));
#else
    free(sub_double_lock);
#endif
    sub_double_lock = NULL;
}
if(mult_double_lock != NULL) {
    sem_destroy(mult_double_lock);
#if 0 == 1
    munlock(mult_double_lock, sizeof(sem_t));
    munmap(mult_double_lock, sizeof(sem_t));
#else
    free(mult_double_lock);
#endif
    mult_double_lock = NULL;
}
if(division_double_lock != NULL) {
    sem_destroy(division_double_lock);
#if 0 == 1
    munlock(division_double_lock, sizeof(sem_t));
    munmap(division_double_lock, sizeof(sem_t));
#else
    free(division_double_lock);
#endif
    division_double_lock = NULL;
}
if(__dupintern__first_a_lock != NULL) {
    sem_destroy(__dupintern__first_a_lock);
#if 0 == 1
    munlock(__dupintern__first_a_lock, sizeof(sem_t));
    munmap(__dupintern__first_a_lock, sizeof(sem_t));
#else
    free(__dupintern__first_a_lock);
#endif
    __dupintern__first_a_lock = NULL;
}
if(__dupintern__first_a_lock != NULL) {
    sem_destroy(__dupintern__first_a_lock);
#if 0 == 1
    munlock(__dupintern__first_a_lock, sizeof(sem_t));
    munmap(__dupintern__first_a_lock, sizeof(sem_t));
#else
    free(__dupintern__first_a_lock);
#endif
    __dupintern__first_a_lock = NULL;
}
if(__dupintern__first_a_lock != NULL) {
    sem_destroy(__dupintern__first_a_lock);
#if 0 == 1
    munlock(__dupintern__first_a_lock, sizeof(sem_t));
    munmap(__dupintern__first_a_lock, sizeof(sem_t));
#else
    free(__dupintern__first_a_lock);
#endif
    __dupintern__first_a_lock = NULL;
}
if(__dupintern__first_a_lock != NULL) {
    sem_destroy(__dupintern__first_a_lock);
#if 0 == 1
    munlock(__dupintern__first_a_lock, sizeof(sem_t));
    munmap(__dupintern__first_a_lock, sizeof(sem_t));
#else
    free(__dupintern__first_a_lock);
#endif
    __dupintern__first_a_lock = NULL;
}
    
    	clean_buffer_int1(&first_a);
	clean_buffer_double1(&add_b);
	clean_buffer_double1(&sub_c);
	clean_buffer_double1(&mult_d);
	clean_buffer_double1(&division_e);
	clean_buffer_double1(&add_double_f);
	clean_buffer_double1(&sub_double_g);
	clean_buffer_double1(&mult_double_h);
	clean_buffer_double1(&division_double_i);
	clean_buffer_int1(&__dupintern__first_a_out4);
	clean_buffer_int1(&__dupintern__first_a_out3);
	clean_buffer_int1(&__dupintern__first_a_out2);
	clean_buffer_int1(&__dupintern__first_a_out1);

}

void main_exit(int unused) {
    main_clean();
    exit(0);
}

int main(int argc, char **argv) 
{
    int status = 0;
    pthread_attr_t attr;
    struct sched_param schedattr;

    status = main_init();
    tryExit(status, "main_init()");

    
    THREAD_INSTANTIATE(first, v1a, first0, 0, 0)
    THREAD_INSTANTIATE(first, v1b, first1, 0, 0)
    THREAD_INSTANTIATE(__dupintern__first_a, , __dupintern__2, 0, 7)
    THREAD_INSTANTIATE(division, v1a, division3, 0, 4)
    THREAD_INSTANTIATE(division, v1b, division4, 0, 4)
    THREAD_INSTANTIATE(mult, v1a, mult5, 0, 6)
    THREAD_INSTANTIATE(mult, v1b, mult6, 0, 6)
    THREAD_INSTANTIATE(add, v1a, add7, 0, 5)
    THREAD_INSTANTIATE(add, v1b, add8, 0, 5)
    THREAD_INSTANTIATE(sub, v1a, sub9, 0, 7)
    THREAD_INSTANTIATE(sub, v1b, sub10, 0, 7)
    THREAD_INSTANTIATE(sub_double, v1a, sub_double11, 0, 7)
    THREAD_INSTANTIATE(sub_double, v1b, sub_double12, 0, 7)
    THREAD_INSTANTIATE(division_double, v1a, division_dou13, 0, 6)
    THREAD_INSTANTIATE(division_double, v1b, division_dou14, 0, 6)
    THREAD_INSTANTIATE(mult_double, v1a, mult_double15, 0, 5)
    THREAD_INSTANTIATE(mult_double, v1b, mult_double16, 0, 5)
    THREAD_INSTANTIATE(add_double, v1a, add_double17, 0, 4)
    THREAD_INSTANTIATE(add_double, v1b, add_double18, 0, 4)
    THREAD_INSTANTIATE(sum_all, v1a, sum_all19, 0, 7)
    THREAD_INSTANTIATE(sum_all, v1b, sum_all20, 0, 7)

    signal(SIGINT, main_exit);
    signal(SIGILL, main_exit);
    signal(SIGABRT, main_exit);
    signal(SIGFPE, main_exit);
    signal(SIGSEGV, main_exit);
    signal(SIGTERM, main_exit);
    signal(SIGKILL, main_exit);
    signal(SIGSTOP, main_exit);
    signal(SIGTSTP, main_exit);

    
    status = pthread_join(th_first_v1a, NULL);
    tryExit(status, "pthread_join(th_first_v1a, NULL)");
    status = pthread_join(th_first_v1b, NULL);
    tryExit(status, "pthread_join(th_first_v1b, NULL)");
    status = pthread_join(th___dupintern__first_a_, NULL);
    tryExit(status, "pthread_join(th___dupintern__first_a_, NULL)");
    status = pthread_join(th_division_v1a, NULL);
    tryExit(status, "pthread_join(th_division_v1a, NULL)");
    status = pthread_join(th_division_v1b, NULL);
    tryExit(status, "pthread_join(th_division_v1b, NULL)");
    status = pthread_join(th_mult_v1a, NULL);
    tryExit(status, "pthread_join(th_mult_v1a, NULL)");
    status = pthread_join(th_mult_v1b, NULL);
    tryExit(status, "pthread_join(th_mult_v1b, NULL)");
    status = pthread_join(th_add_v1a, NULL);
    tryExit(status, "pthread_join(th_add_v1a, NULL)");
    status = pthread_join(th_add_v1b, NULL);
    tryExit(status, "pthread_join(th_add_v1b, NULL)");
    status = pthread_join(th_sub_v1a, NULL);
    tryExit(status, "pthread_join(th_sub_v1a, NULL)");
    status = pthread_join(th_sub_v1b, NULL);
    tryExit(status, "pthread_join(th_sub_v1b, NULL)");
    status = pthread_join(th_sub_double_v1a, NULL);
    tryExit(status, "pthread_join(th_sub_double_v1a, NULL)");
    status = pthread_join(th_sub_double_v1b, NULL);
    tryExit(status, "pthread_join(th_sub_double_v1b, NULL)");
    status = pthread_join(th_division_double_v1a, NULL);
    tryExit(status, "pthread_join(th_division_double_v1a, NULL)");
    status = pthread_join(th_division_double_v1b, NULL);
    tryExit(status, "pthread_join(th_division_double_v1b, NULL)");
    status = pthread_join(th_mult_double_v1a, NULL);
    tryExit(status, "pthread_join(th_mult_double_v1a, NULL)");
    status = pthread_join(th_mult_double_v1b, NULL);
    tryExit(status, "pthread_join(th_mult_double_v1b, NULL)");
    status = pthread_join(th_add_double_v1a, NULL);
    tryExit(status, "pthread_join(th_add_double_v1a, NULL)");
    status = pthread_join(th_add_double_v1b, NULL);
    tryExit(status, "pthread_join(th_add_double_v1b, NULL)");
    status = pthread_join(th_sum_all_v1a, NULL);
    tryExit(status, "pthread_join(th_sum_all_v1a, NULL)");
    status = pthread_join(th_sum_all_v1b, NULL);
    tryExit(status, "pthread_join(th_sum_all_v1b, NULL)");

    main_clean();

    printf("Done, ready to exit\n");
    return status;
}

