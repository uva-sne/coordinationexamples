#include "ToyExample.h"

void mult_double(int in, double* h){
    printf("input number: %d from task: mult_double\n", in);

    double a=2.0D;
    double b=6.54D;

    for (size_t i = 0; i < NUM_ITER; i++) {
        a *= b;
    }

    printf("Results from mult_double are: %f \n", a);
    *h= 8.0D;
}
