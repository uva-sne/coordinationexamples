
#pragma once
#ifndef SHAREDDEFS_H
#define SHAREDDEFS_H

#include "ToyExampleMatrix.h"


#ifdef __cplusplus
typedef bool BOOL;
#define TRUE true
#define FALSE false
#else
typedef short BOOL;
#define TRUE 1
#define FALSE 0
#endif


typedef struct {
   uint32_t head;
   uint32_t tail;
   matrix_int_t buffer[1+1];//add +1 to the size as a circular buffer of size 1 doesn't work
} buffer_matrix_int_t1_t;
static inline void init_buffer_matrix_int_t1(buffer_matrix_int_t1_t *buffer) {
    buffer->head = buffer->tail = 0;
}
static inline void clean_buffer_matrix_int_t1(buffer_matrix_int_t1_t *buffer) {
}
static inline BOOL token_available_matrix_int_t1(buffer_matrix_int_t1_t *buffer) {
    return (buffer->tail != buffer->head);
}
static inline BOOL peek_matrix_int_t1(buffer_matrix_int_t1_t *buffer, matrix_int_t* res, int offset=0) {
    if(buffer->tail == buffer->head)
        return FALSE;
    *res = (buffer->buffer)[(buffer->tail+offset)%(1+1)];
    return TRUE;
}
static inline BOOL pop_matrix_int_t1(buffer_matrix_int_t1_t *buffer, matrix_int_t* res) {
    if(buffer->tail == buffer->head)
        return FALSE;
    if(res != NULL)
        *res = (buffer->buffer)[buffer->tail];
    buffer->tail = (buffer->tail+1)%(1+1);
    return TRUE;
}
static inline void push_matrix_int_t1(buffer_matrix_int_t1_t *buffer, matrix_int_t val) {
    (buffer->buffer)[buffer->head] = val;
    buffer->head = (buffer->head+1)%(1+1);
}
static inline void insert_matrix_int_t1(buffer_matrix_int_t1_t *buffer, matrix_int_t val, int offset=0) {
    (buffer->buffer)[buffer->tail+offset] = val;
}


#endif //SHAREDDEFS_H
