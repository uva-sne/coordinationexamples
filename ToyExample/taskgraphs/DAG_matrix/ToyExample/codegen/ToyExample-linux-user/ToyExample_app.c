
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <semaphore.h> 
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <sched.h>
#include <sys/wait.h>

#include "ToyExampleMatrix.h"
#include "shared_defs.h"


static inline int sem_wait_nointr(sem_t *sem) {
    while (sem_wait(sem)) {
        if (errno == EINTR) errno = 0;
        else return -1;
    }
    return 0;
}
void main_clean();

#define errExit(msg) { perror(msg); main_clean(); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) {printf("-> %d <-\n",var); errno = var ; errExit(msg) }
#define STRINGIFY(X) #X
#define CONCAT(W,X,Y,Z) W ## X ## Y ## Z

buffer_matrix_int_t1_t first_a;
buffer_matrix_int_t1_t *add_a;
buffer_matrix_int_t1_t add_b;
buffer_matrix_int_t1_t *sub_a;
buffer_matrix_int_t1_t sub_c;
buffer_matrix_int_t1_t *mult_a;
buffer_matrix_int_t1_t mult_d;
buffer_matrix_int_t1_t *division_a;
buffer_matrix_int_t1_t division_e;
buffer_matrix_int_t1_t *square_matrix_d;
buffer_matrix_int_t1_t square_matrix_f;
buffer_matrix_int_t1_t *mult_matrix_b;
buffer_matrix_int_t1_t *mult_matrix_c;
buffer_matrix_int_t1_t mult_matrix_g;
buffer_matrix_int_t1_t *transpose_matrix_e;
buffer_matrix_int_t1_t transpose_matrix_h;
buffer_matrix_int_t1_t *sum_all_f;
buffer_matrix_int_t1_t *sum_all_g;
buffer_matrix_int_t1_t *sum_all_h;
buffer_matrix_int_t1_t *__dupintern__first_a_in;
buffer_matrix_int_t1_t __dupintern__first_a_out4;
buffer_matrix_int_t1_t __dupintern__first_a_out3;
buffer_matrix_int_t1_t __dupintern__first_a_out2;
buffer_matrix_int_t1_t __dupintern__first_a_out1;





void __coord_first_v1a() {
    int status;

    

    matrix_int_t a;
matrix_int_t  __bkinit__a = a;

    first(&a);
    
if(a != __bkinit__a)
    push_matrix_int_t1(&first_a, a);


    
}

void __coord_first_v1b() {
    int status;

    

    matrix_int_t a;
matrix_int_t  __bkinit__a = a;

    first(&a);
    
if(a != __bkinit__a)
    push_matrix_int_t1(&first_a, a);


    
}

void __coord_first_v1c() {
    int status;

    

    matrix_int_t a;
matrix_int_t  __bkinit__a = a;

    first_v1c(&a);
    
if(a != __bkinit__a)
    push_matrix_int_t1(&first_a, a);


    
}

void __coord_add_v1a() {
    int status;

    
if(!token_available_matrix_int_t1(add_a))
    return;


    matrix_int_t a;
matrix_int_t b;
matrix_int_t  __bkinit__b = b;
pop_matrix_int_t1(add_a, &a);

    add(a, &b);
    
if(b != __bkinit__b)
    push_matrix_int_t1(&add_b, b);


    
}

void __coord_add_v1b() {
    int status;

    
if(!token_available_matrix_int_t1(add_a))
    return;


    matrix_int_t a;
matrix_int_t b;
matrix_int_t  __bkinit__b = b;
pop_matrix_int_t1(add_a, &a);

    add(a, &b);
    
if(b != __bkinit__b)
    push_matrix_int_t1(&add_b, b);


    
}

void __coord_add_v1c() {
    int status;

    
if(!token_available_matrix_int_t1(add_a))
    return;


    matrix_int_t a;
matrix_int_t b;
matrix_int_t  __bkinit__b = b;
pop_matrix_int_t1(add_a, &a);

    add_v1c(a, &b);
    
if(b != __bkinit__b)
    push_matrix_int_t1(&add_b, b);


    
}

void __coord_sub_v1a() {
    int status;

    
if(!token_available_matrix_int_t1(sub_a))
    return;


    matrix_int_t a;
matrix_int_t c;
matrix_int_t  __bkinit__c = c;
pop_matrix_int_t1(sub_a, &a);

    sub(a, &c);
    
if(c != __bkinit__c)
    push_matrix_int_t1(&sub_c, c);


    
}

void __coord_sub_v1b() {
    int status;

    
if(!token_available_matrix_int_t1(sub_a))
    return;


    matrix_int_t a;
matrix_int_t c;
matrix_int_t  __bkinit__c = c;
pop_matrix_int_t1(sub_a, &a);

    sub(a, &c);
    
if(c != __bkinit__c)
    push_matrix_int_t1(&sub_c, c);


    
}

void __coord_sub_v1c() {
    int status;

    
if(!token_available_matrix_int_t1(sub_a))
    return;


    matrix_int_t a;
matrix_int_t c;
matrix_int_t  __bkinit__c = c;
pop_matrix_int_t1(sub_a, &a);

    sub_v1c(a, &c);
    
if(c != __bkinit__c)
    push_matrix_int_t1(&sub_c, c);


    
}

void __coord_mult_v1a() {
    int status;

    
if(!token_available_matrix_int_t1(mult_a))
    return;


    matrix_int_t a;
matrix_int_t d;
matrix_int_t  __bkinit__d = d;
pop_matrix_int_t1(mult_a, &a);

    mult(a, &d);
    
if(d != __bkinit__d)
    push_matrix_int_t1(&mult_d, d);


    
}

void __coord_mult_v1b() {
    int status;

    
if(!token_available_matrix_int_t1(mult_a))
    return;


    matrix_int_t a;
matrix_int_t d;
matrix_int_t  __bkinit__d = d;
pop_matrix_int_t1(mult_a, &a);

    mult(a, &d);
    
if(d != __bkinit__d)
    push_matrix_int_t1(&mult_d, d);


    
}

void __coord_mult_v1c() {
    int status;

    
if(!token_available_matrix_int_t1(mult_a))
    return;


    matrix_int_t a;
matrix_int_t d;
matrix_int_t  __bkinit__d = d;
pop_matrix_int_t1(mult_a, &a);

    mult_v1c(a, &d);
    
if(d != __bkinit__d)
    push_matrix_int_t1(&mult_d, d);


    
}

void __coord_division_v1a() {
    int status;

    
if(!token_available_matrix_int_t1(division_a))
    return;


    matrix_int_t a;
matrix_int_t e;
matrix_int_t  __bkinit__e = e;
pop_matrix_int_t1(division_a, &a);

    division(a, &e);
    
if(e != __bkinit__e)
    push_matrix_int_t1(&division_e, e);


    
}

void __coord_division_v1b() {
    int status;

    
if(!token_available_matrix_int_t1(division_a))
    return;


    matrix_int_t a;
matrix_int_t e;
matrix_int_t  __bkinit__e = e;
pop_matrix_int_t1(division_a, &a);

    division(a, &e);
    
if(e != __bkinit__e)
    push_matrix_int_t1(&division_e, e);


    
}

void __coord_division_v1c() {
    int status;

    
if(!token_available_matrix_int_t1(division_a))
    return;


    matrix_int_t a;
matrix_int_t e;
matrix_int_t  __bkinit__e = e;
pop_matrix_int_t1(division_a, &a);

    division_v1c(a, &e);
    
if(e != __bkinit__e)
    push_matrix_int_t1(&division_e, e);


    
}

void __coord_square_matrix_v1a() {
    int status;

    
if(!token_available_matrix_int_t1(square_matrix_d))
    return;


    matrix_int_t d;
matrix_int_t f;
matrix_int_t  __bkinit__f = f;
pop_matrix_int_t1(square_matrix_d, &d);

    square_matrix(d, &f);
    
if(f != __bkinit__f)
    push_matrix_int_t1(&square_matrix_f, f);


    
}

void __coord_square_matrix_v1b() {
    int status;

    
if(!token_available_matrix_int_t1(square_matrix_d))
    return;


    matrix_int_t d;
matrix_int_t f;
matrix_int_t  __bkinit__f = f;
pop_matrix_int_t1(square_matrix_d, &d);

    square_matrix(d, &f);
    
if(f != __bkinit__f)
    push_matrix_int_t1(&square_matrix_f, f);


    
}

void __coord_square_matrix_v1c() {
    int status;

    
if(!token_available_matrix_int_t1(square_matrix_d))
    return;


    matrix_int_t d;
matrix_int_t f;
matrix_int_t  __bkinit__f = f;
pop_matrix_int_t1(square_matrix_d, &d);

    square_matrix_v1c(d, &f);
    
if(f != __bkinit__f)
    push_matrix_int_t1(&square_matrix_f, f);


    
}

void __coord_mult_matrix_v1a() {
    int status;

    
if(!token_available_matrix_int_t1(mult_matrix_b))
    return;

if(!token_available_matrix_int_t1(mult_matrix_c))
    return;


    matrix_int_t b;
matrix_int_t c;
matrix_int_t g;
matrix_int_t  __bkinit__g = g;
pop_matrix_int_t1(mult_matrix_b, &b);
pop_matrix_int_t1(mult_matrix_c, &c);

    mult_matrix(b, c, &g);
    
if(g != __bkinit__g)
    push_matrix_int_t1(&mult_matrix_g, g);


    
}

void __coord_mult_matrix_v1b() {
    int status;

    
if(!token_available_matrix_int_t1(mult_matrix_b))
    return;

if(!token_available_matrix_int_t1(mult_matrix_c))
    return;


    matrix_int_t b;
matrix_int_t c;
matrix_int_t g;
matrix_int_t  __bkinit__g = g;
pop_matrix_int_t1(mult_matrix_b, &b);
pop_matrix_int_t1(mult_matrix_c, &c);

    mult_matrix(b, c, &g);
    
if(g != __bkinit__g)
    push_matrix_int_t1(&mult_matrix_g, g);


    
}

void __coord_mult_matrix_v1c() {
    int status;

    
if(!token_available_matrix_int_t1(mult_matrix_b))
    return;

if(!token_available_matrix_int_t1(mult_matrix_c))
    return;


    matrix_int_t b;
matrix_int_t c;
matrix_int_t g;
matrix_int_t  __bkinit__g = g;
pop_matrix_int_t1(mult_matrix_b, &b);
pop_matrix_int_t1(mult_matrix_c, &c);

    mult_matrix_v1c(b, c, &g);
    
if(g != __bkinit__g)
    push_matrix_int_t1(&mult_matrix_g, g);


    
}

void __coord_transpose_matrix_v1a() {
    int status;

    
if(!token_available_matrix_int_t1(transpose_matrix_e))
    return;


    matrix_int_t e;
matrix_int_t h;
matrix_int_t  __bkinit__h = h;
pop_matrix_int_t1(transpose_matrix_e, &e);

    transpose_matrix(e, &h);
    
if(h != __bkinit__h)
    push_matrix_int_t1(&transpose_matrix_h, h);


    
}

void __coord_transpose_matrix_v1b() {
    int status;

    
if(!token_available_matrix_int_t1(transpose_matrix_e))
    return;


    matrix_int_t e;
matrix_int_t h;
matrix_int_t  __bkinit__h = h;
pop_matrix_int_t1(transpose_matrix_e, &e);

    transpose_matrix(e, &h);
    
if(h != __bkinit__h)
    push_matrix_int_t1(&transpose_matrix_h, h);


    
}

void __coord_transpose_matrix_v1c() {
    int status;

    
if(!token_available_matrix_int_t1(transpose_matrix_e))
    return;


    matrix_int_t e;
matrix_int_t h;
matrix_int_t  __bkinit__h = h;
pop_matrix_int_t1(transpose_matrix_e, &e);

    transpose_matrix_v1c(e, &h);
    
if(h != __bkinit__h)
    push_matrix_int_t1(&transpose_matrix_h, h);


    
}

void __coord_sum_all_v1a() {
    int status;

    
if(!token_available_matrix_int_t1(sum_all_f))
    return;

if(!token_available_matrix_int_t1(sum_all_g))
    return;

if(!token_available_matrix_int_t1(sum_all_h))
    return;


    matrix_int_t f;
matrix_int_t g;
matrix_int_t h;
pop_matrix_int_t1(sum_all_f, &f);
pop_matrix_int_t1(sum_all_g, &g);
pop_matrix_int_t1(sum_all_h, &h);

    sum_all(f, g, h);
    

    
}

void __coord_sum_all_v1b() {
    int status;

    
if(!token_available_matrix_int_t1(sum_all_f))
    return;

if(!token_available_matrix_int_t1(sum_all_g))
    return;

if(!token_available_matrix_int_t1(sum_all_h))
    return;


    matrix_int_t f;
matrix_int_t g;
matrix_int_t h;
pop_matrix_int_t1(sum_all_f, &f);
pop_matrix_int_t1(sum_all_g, &g);
pop_matrix_int_t1(sum_all_h, &h);

    sum_all(f, g, h);
    

    
}

void __coord_sum_all_v1c() {
    int status;

    
if(!token_available_matrix_int_t1(sum_all_f))
    return;

if(!token_available_matrix_int_t1(sum_all_g))
    return;

if(!token_available_matrix_int_t1(sum_all_h))
    return;


    matrix_int_t f;
matrix_int_t g;
matrix_int_t h;
pop_matrix_int_t1(sum_all_f, &f);
pop_matrix_int_t1(sum_all_g, &g);
pop_matrix_int_t1(sum_all_h, &h);

    sum_all_v1c(f, g, h);
    

    
}

void __coord___dupintern__first_a_() {
    int status;

    
if(!token_available_matrix_int_t1(__dupintern__first_a_in))
    return;


    matrix_int_t in;
matrix_int_t out4;
matrix_int_t  __bkinit__out4 = out4;
matrix_int_t out3;
matrix_int_t  __bkinit__out3 = out3;
matrix_int_t out2;
matrix_int_t  __bkinit__out2 = out2;
matrix_int_t out1;
matrix_int_t  __bkinit__out1 = out1;
pop_matrix_int_t1(__dupintern__first_a_in, &in);

    out4 = in;
out3 = in;
out2 = in;
out1 = in;

    
if(out4 != __bkinit__out4)
    push_matrix_int_t1(&__dupintern__first_a_out4, out4);

if(out3 != __bkinit__out3)
    push_matrix_int_t1(&__dupintern__first_a_out3, out3);

if(out2 != __bkinit__out2)
    push_matrix_int_t1(&__dupintern__first_a_out2, out2);

if(out1 != __bkinit__out1)
    push_matrix_int_t1(&__dupintern__first_a_out1, out1);


    
}


void main_exit(int);
int main_init() {
    int status = 0;

    signal(SIGINT, main_exit);
    signal(SIGILL, main_exit);
    signal(SIGABRT, main_exit);
    signal(SIGFPE, main_exit);
    signal(SIGSEGV, main_exit);
    signal(SIGTERM, main_exit);
    signal(SIGKILL, main_exit);
    signal(SIGSTOP, main_exit);
    signal(SIGTSTP, main_exit);

    	init_buffer_matrix_int_t1(&first_a);
	init_buffer_matrix_int_t1(&add_b);
	init_buffer_matrix_int_t1(&sub_c);
	init_buffer_matrix_int_t1(&mult_d);
	init_buffer_matrix_int_t1(&division_e);
	init_buffer_matrix_int_t1(&square_matrix_f);
	init_buffer_matrix_int_t1(&mult_matrix_g);
	init_buffer_matrix_int_t1(&transpose_matrix_h);
	init_buffer_matrix_int_t1(&__dupintern__first_a_out4);
	init_buffer_matrix_int_t1(&__dupintern__first_a_out3);
	init_buffer_matrix_int_t1(&__dupintern__first_a_out2);
	init_buffer_matrix_int_t1(&__dupintern__first_a_out1);
	add_a = &__dupintern__first_a_out1;
	sub_a = &__dupintern__first_a_out2;
	mult_a = &__dupintern__first_a_out3;
	division_a = &__dupintern__first_a_out4;
	square_matrix_d = &mult_d;
	mult_matrix_b = &add_b;
	mult_matrix_c = &sub_c;
	transpose_matrix_e = &division_e;
	sum_all_f = &square_matrix_f;
	sum_all_g = &mult_matrix_g;
	sum_all_h = &transpose_matrix_h;
	__dupintern__first_a_in = &first_a;


    

    init_gpu();

    return status;
}

void main_clean() {
    int status = 0;

    	clean_buffer_matrix_int_t1(&first_a);
	clean_buffer_matrix_int_t1(&add_b);
	clean_buffer_matrix_int_t1(&sub_c);
	clean_buffer_matrix_int_t1(&mult_d);
	clean_buffer_matrix_int_t1(&division_e);
	clean_buffer_matrix_int_t1(&square_matrix_f);
	clean_buffer_matrix_int_t1(&mult_matrix_g);
	clean_buffer_matrix_int_t1(&transpose_matrix_h);
	clean_buffer_matrix_int_t1(&__dupintern__first_a_out4);
	clean_buffer_matrix_int_t1(&__dupintern__first_a_out3);
	clean_buffer_matrix_int_t1(&__dupintern__first_a_out2);
	clean_buffer_matrix_int_t1(&__dupintern__first_a_out1);


    

    
}

void main_exit(int unused) {
    main_clean();
    exit(0);
}

int main(int argc, char **argv) 
{
    int status = 0;

    main_init();

    

    

    for(;;) {
        __coord_first_v1a();
__coord_first_v1b();
__coord_first_v1c();
__coord___dupintern__first_a_();
__coord_division_v1a();
__coord_division_v1b();
__coord_division_v1c();
__coord_mult_v1a();
__coord_mult_v1b();
__coord_mult_v1c();
__coord_add_v1a();
__coord_add_v1b();
__coord_add_v1c();
__coord_sub_v1a();
__coord_sub_v1b();
__coord_sub_v1c();
__coord_transpose_matrix_v1a();
__coord_transpose_matrix_v1b();
__coord_transpose_matrix_v1c();
__coord_square_matrix_v1a();
__coord_square_matrix_v1b();
__coord_square_matrix_v1c();
__coord_mult_matrix_v1a();
__coord_mult_matrix_v1b();
__coord_mult_matrix_v1c();
__coord_sum_all_v1a();
__coord_sum_all_v1b();
__coord_sum_all_v1c();

    }

    main_clean();
    printf("Done, ready to exit\n");
    return status;
}

