
// START user-space/AppSequential.tpl
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <semaphore.h> 
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <sched.h>
#include <sys/wait.h>

#include "ToyExample.h"
#include "odroid_uart.h"
#include "shared_defs.h"

static inline int sem_wait_nointr(sem_t *sem) {
    while (sem_wait(sem)) {
        if (errno == EINTR) errno = 0;
        else return -1;
    }
    return 0;
}
void main_clean();

#define errExit(msg) { perror(msg); main_clean(); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) {printf("-> %d <-\n",var); errno = var ; errExit(msg) }
#define STRINGIFY(X) #X
#define CONCAT(W,X,Y,Z) W ## X ## Y ## Z

buffer_int1_t *add_a;
buffer_double1_t add_b;
buffer_int1_t *sub_a;
buffer_double1_t sub_c;
buffer_int1_t *mult_a;
buffer_double1_t mult_d;
buffer_int1_t *division_a;
buffer_double1_t division_e;
buffer_double1_t *add_double_c;
buffer_double1_t add_double_f;
buffer_double1_t *sub_double_e;
buffer_double1_t sub_double_g;
buffer_double1_t *mult_double_b;
buffer_double1_t mult_double_h;
buffer_double1_t *division_double_d;
buffer_double1_t division_double_i;
buffer_double1_t *sum_all_f;
buffer_double1_t *sum_all_g;
buffer_double1_t *sum_all_h;
buffer_double1_t *sum_all_i;
buffer_int1_t first_a;
buffer_int1_t *__dupintern__first_a_in;
buffer_int1_t __dupintern__first_a_out4;
buffer_int1_t __dupintern__first_a_out3;
buffer_int1_t __dupintern__first_a_out2;
buffer_int1_t __dupintern__first_a_out1;



// START TaskMgnt/SemDecl.tpl
sem_t *add_lock = NULL;
// END TaskMgnt/SemDecl.tpl

// START TaskMgnt/SemDecl.tpl
sem_t *sub_lock = NULL;
// END TaskMgnt/SemDecl.tpl

// START TaskMgnt/SemDecl.tpl
sem_t *mult_lock = NULL;
// END TaskMgnt/SemDecl.tpl

// START TaskMgnt/SemDecl.tpl
sem_t *division_lock = NULL;
// END TaskMgnt/SemDecl.tpl

// START TaskMgnt/SemDecl.tpl
sem_t *add_double_lock = NULL;
// END TaskMgnt/SemDecl.tpl

// START TaskMgnt/SemDecl.tpl
sem_t *sub_double_lock = NULL;
// END TaskMgnt/SemDecl.tpl

// START TaskMgnt/SemDecl.tpl
sem_t *mult_double_lock = NULL;
// END TaskMgnt/SemDecl.tpl

// START TaskMgnt/SemDecl.tpl
sem_t *division_double_lock = NULL;
// END TaskMgnt/SemDecl.tpl

// START TaskMgnt/SemDecl.tpl
sem_t *first_lock = NULL;
// END TaskMgnt/SemDecl.tpl

// START TaskMgnt/SemDecl.tpl
sem_t *__dupintern__first_a_lock = NULL;
// END TaskMgnt/SemDecl.tpl



// START TaskMgnt/SemTask.tpl

void __coord_add_v1a() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if (status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    int a = 0;
    double b = 0;
    double __bkinit__b = b;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_int1(add_a, &a);
    // END TaskMgnt/TaskInputTokens.tpl

    add(a, &b);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (b != __bkinit__b)
        push_double1(&add_b, b);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(add_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_add_v1b() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if (status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    int a = 0;
    double b = 0;
    double __bkinit__b = b;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_int1(add_a, &a);
    // END TaskMgnt/TaskInputTokens.tpl

    add(a, &b);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (b != __bkinit__b)
        push_double1(&add_b, b);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(add_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_sub_v1a() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if (status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    int a = 0;
    double c = 0;
    double __bkinit__c = c;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_int1(sub_a, &a);
    // END TaskMgnt/TaskInputTokens.tpl

    sub(a, &c);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (c != __bkinit__c)
        push_double1(&sub_c, c);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(sub_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_sub_v1b() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if (status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    int a = 0;
    double c = 0;
    double __bkinit__c = c;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_int1(sub_a, &a);
    // END TaskMgnt/TaskInputTokens.tpl

    sub(a, &c);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (c != __bkinit__c)
        push_double1(&sub_c, c);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(sub_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_mult_v1a() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if (status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    int a = 0;
    double d = 0;
    double __bkinit__d = d;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_int1(mult_a, &a);
    // END TaskMgnt/TaskInputTokens.tpl

    mult(a, &d);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (d != __bkinit__d)
        push_double1(&mult_d, d);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(mult_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_mult_v1b() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if (status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    int a = 0;
    double d = 0;
    double __bkinit__d = d;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_int1(mult_a, &a);
    // END TaskMgnt/TaskInputTokens.tpl

    mult(a, &d);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (d != __bkinit__d)
        push_double1(&mult_d, d);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(mult_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_division_v1a() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if (status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    int a = 0;
    double e = 0;
    double __bkinit__e = e;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_int1(division_a, &a);
    // END TaskMgnt/TaskInputTokens.tpl

    division(a, &e);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (e != __bkinit__e)
        push_double1(&division_e, e);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(division_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_division_v1b() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(__dupintern__first_a_lock);
    if (status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    int a = 0;
    double e = 0;
    double __bkinit__e = e;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_int1(division_a, &a);
    // END TaskMgnt/TaskInputTokens.tpl

    division(a, &e);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (e != __bkinit__e)
        push_double1(&division_e, e);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(division_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_add_double_v1a() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(sub_lock);
    if (status != 0) {
        perror("sem_wait_nointr(sub_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double c = 0;
    double f = 0;
    double __bkinit__f = f;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(add_double_c, &c);
    // END TaskMgnt/TaskInputTokens.tpl

    add_double(c, &f);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (f != __bkinit__f)
        push_double1(&add_double_f, f);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(add_double_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_add_double_v1b() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(sub_lock);
    if (status != 0) {
        perror("sem_wait_nointr(sub_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double c = 0;
    double f = 0;
    double __bkinit__f = f;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(add_double_c, &c);
    // END TaskMgnt/TaskInputTokens.tpl

    add_double(c, &f);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (f != __bkinit__f)
        push_double1(&add_double_f, f);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(add_double_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_sub_double_v1a() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(division_lock);
    if (status != 0) {
        perror("sem_wait_nointr(division_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double e = 0;
    double g = 0;
    double __bkinit__g = g;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sub_double_e, &e);
    // END TaskMgnt/TaskInputTokens.tpl

    sub_double(e, &g);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (g != __bkinit__g)
        push_double1(&sub_double_g, g);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(sub_double_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_sub_double_v1b() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(division_lock);
    if (status != 0) {
        perror("sem_wait_nointr(division_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double e = 0;
    double g = 0;
    double __bkinit__g = g;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sub_double_e, &e);
    // END TaskMgnt/TaskInputTokens.tpl

    sub_double(e, &g);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (g != __bkinit__g)
        push_double1(&sub_double_g, g);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(sub_double_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_mult_double_v1a() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(add_lock);
    if (status != 0) {
        perror("sem_wait_nointr(add_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double b = 0;
    double h = 0;
    double __bkinit__h = h;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(mult_double_b, &b);
    // END TaskMgnt/TaskInputTokens.tpl

    mult_double(b, &h);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (h != __bkinit__h)
        push_double1(&mult_double_h, h);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(mult_double_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_mult_double_v1b() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(add_lock);
    if (status != 0) {
        perror("sem_wait_nointr(add_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double b = 0;
    double h = 0;
    double __bkinit__h = h;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(mult_double_b, &b);
    // END TaskMgnt/TaskInputTokens.tpl

    mult_double(b, &h);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (h != __bkinit__h)
        push_double1(&mult_double_h, h);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(mult_double_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_division_double_v1a() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(mult_lock);
    if (status != 0) {
        perror("sem_wait_nointr(mult_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double d = 0;
    double i = 0;
    double __bkinit__i = i;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(division_double_d, &d);
    // END TaskMgnt/TaskInputTokens.tpl

    division_double(d, &i);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (i != __bkinit__i)
        push_double1(&division_double_i, i);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(division_double_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_division_double_v1b() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(mult_lock);
    if (status != 0) {
        perror("sem_wait_nointr(mult_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double d = 0;
    double i = 0;
    double __bkinit__i = i;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(division_double_d, &d);
    // END TaskMgnt/TaskInputTokens.tpl

    division_double(d, &i);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (i != __bkinit__i)
        push_double1(&division_double_i, i);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(division_double_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_sum_all_v1a() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(add_double_lock);
    if (status != 0) {
        perror("sem_wait_nointr(add_double_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl

    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(sub_double_lock);
    if (status != 0) {
        perror("sem_wait_nointr(sub_double_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl

    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(mult_double_lock);
    if (status != 0) {
        perror("sem_wait_nointr(mult_double_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl

    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(division_double_lock);
    if (status != 0) {
        perror("sem_wait_nointr(division_double_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double f = 0;
    double g = 0;
    double h = 0;
    double i = 0;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sum_all_f, &f);
    // END TaskMgnt/TaskInputTokens.tpl

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sum_all_g, &g);
    // END TaskMgnt/TaskInputTokens.tpl

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sum_all_h, &h);
    // END TaskMgnt/TaskInputTokens.tpl

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sum_all_i, &i);
    // END TaskMgnt/TaskInputTokens.tpl

    sum_all(f, g, h, i);


#if 0 > 0
    for (int __i__ = 0; __i__ < 0; ++__i__)
        sem_post(sum_all_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_sum_all_v1b() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(add_double_lock);
    if (status != 0) {
        perror("sem_wait_nointr(add_double_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl

    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(sub_double_lock);
    if (status != 0) {
        perror("sem_wait_nointr(sub_double_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl

    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(mult_double_lock);
    if (status != 0) {
        perror("sem_wait_nointr(mult_double_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl

    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(division_double_lock);
    if (status != 0) {
        perror("sem_wait_nointr(division_double_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    double f = 0;
    double g = 0;
    double h = 0;
    double i = 0;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sum_all_f, &f);
    // END TaskMgnt/TaskInputTokens.tpl

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sum_all_g, &g);
    // END TaskMgnt/TaskInputTokens.tpl

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sum_all_h, &h);
    // END TaskMgnt/TaskInputTokens.tpl

    // START TaskMgnt/TaskInputTokens.tpl
    pop_double1(sum_all_i, &i);
    // END TaskMgnt/TaskInputTokens.tpl

    sum_all(f, g, h, i);


#if 0 > 0
    for (int __i__ = 0; __i__ < 0; ++__i__)
        sem_post(sum_all_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_first_v1a() {
    int status;



    int a = 0;
    int __bkinit__a = a;

    first(&a);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (a != __bkinit__a)
        push_int1(&first_a, a);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(first_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord_first_v1b() {
    int status;



    int a = 0;
    int __bkinit__a = a;

    first(&a);

    // START TaskMgnt/TaskOutputTokens.tpl
    if (a != __bkinit__a)
        push_int1(&first_a, a);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 1 > 0
    for (int __i__ = 0; __i__ < 1; ++__i__)
        sem_post(first_lock);
#endif
}
// END TaskMgnt/SemTask.tpl

// START TaskMgnt/SemTask.tpl

void __coord___dupintern__first_a_() {
    int status;


    // START TaskMgnt/SemTaskPred.tpl
    status = sem_wait_nointr(first_lock);
    if (status != 0) {
        perror("sem_wait_nointr(first_lock)");
        return;
    }
    // END TaskMgnt/SemTaskPred.tpl


    int in = 0;
    int out4 = 0;
    int __bkinit__out4 = out4;
    int out3 = 0;
    int __bkinit__out3 = out3;
    int out2 = 0;
    int __bkinit__out2 = out2;
    int out1 = 0;
    int __bkinit__out1 = out1;

    // START TaskMgnt/TaskInputTokens.tpl
    pop_int1(__dupintern__first_a_in, &in);
    // END TaskMgnt/TaskInputTokens.tpl

    out4 = in;
    out3 = in;
    out2 = in;
    out1 = in;


    // START TaskMgnt/TaskOutputTokens.tpl
    if (out4 != __bkinit__out4)
        push_int1(&__dupintern__first_a_out4, out4);
    // END TaskMgnt/TaskOutputTokens.tpl

    // START TaskMgnt/TaskOutputTokens.tpl
    if (out3 != __bkinit__out3)
        push_int1(&__dupintern__first_a_out3, out3);
    // END TaskMgnt/TaskOutputTokens.tpl

    // START TaskMgnt/TaskOutputTokens.tpl
    if (out2 != __bkinit__out2)
        push_int1(&__dupintern__first_a_out2, out2);
    // END TaskMgnt/TaskOutputTokens.tpl

    // START TaskMgnt/TaskOutputTokens.tpl
    if (out1 != __bkinit__out1)
        push_int1(&__dupintern__first_a_out1, out1);
    // END TaskMgnt/TaskOutputTokens.tpl


#if 4 > 0
    for (int __i__ = 0; __i__ < 4; ++__i__)
        sem_post(__dupintern__first_a_lock);
#endif
}
// END TaskMgnt/SemTask.tpl


void main_exit(int);

int main_init() {
    int status = 0;

    signal(SIGINT, main_exit);
    signal(SIGILL, main_exit);
    signal(SIGABRT, main_exit);
    signal(SIGFPE, main_exit);
    signal(SIGSEGV, main_exit);
    signal(SIGTERM, main_exit);
    signal(SIGKILL, main_exit);
    signal(SIGSTOP, main_exit);
    signal(SIGTSTP, main_exit);

    init_buffer_double1(&add_b);
    init_buffer_double1(&sub_c);
    init_buffer_double1(&mult_d);
    init_buffer_double1(&division_e);
    init_buffer_double1(&add_double_f);
    init_buffer_double1(&sub_double_g);
    init_buffer_double1(&mult_double_h);
    init_buffer_double1(&division_double_i);
    init_buffer_int1(&first_a);
    init_buffer_int1(&__dupintern__first_a_out4);
    init_buffer_int1(&__dupintern__first_a_out3);
    init_buffer_int1(&__dupintern__first_a_out2);
    init_buffer_int1(&__dupintern__first_a_out1);
    add_a = &__dupintern__first_a_out1;
    sub_a = &__dupintern__first_a_out2;
    mult_a = &__dupintern__first_a_out3;
    division_a = &__dupintern__first_a_out4;
    add_double_c = &sub_c;
    sub_double_e = &division_e;
    mult_double_b = &add_b;
    division_double_d = &mult_d;
    sum_all_f = &add_double_f;
    sum_all_g = &sub_double_g;
    sum_all_h = &mult_double_h;
    sum_all_i = &division_double_i;
    __dupintern__first_a_in = &first_a;



    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    add_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (add_lock == NULL);
    tryExit(status, "mmap add_lock");
    status = mlock(add_lock, sizeof (sem_t));
    tryExit(status, "mlock add_lock");
#else
    add_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(add_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(add_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    sub_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (sub_lock == NULL);
    tryExit(status, "mmap sub_lock");
    status = mlock(sub_lock, sizeof (sem_t));
    tryExit(status, "mlock sub_lock");
#else
    sub_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(sub_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(sub_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    mult_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (mult_lock == NULL);
    tryExit(status, "mmap mult_lock");
    status = mlock(mult_lock, sizeof (sem_t));
    tryExit(status, "mlock mult_lock");
#else
    mult_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(mult_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(mult_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    division_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (division_lock == NULL);
    tryExit(status, "mmap division_lock");
    status = mlock(division_lock, sizeof (sem_t));
    tryExit(status, "mlock division_lock");
#else
    division_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(division_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(division_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    add_double_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (add_double_lock == NULL);
    tryExit(status, "mmap add_double_lock");
    status = mlock(add_double_lock, sizeof (sem_t));
    tryExit(status, "mlock add_double_lock");
#else
    add_double_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(add_double_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(add_double_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    sub_double_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (sub_double_lock == NULL);
    tryExit(status, "mmap sub_double_lock");
    status = mlock(sub_double_lock, sizeof (sem_t));
    tryExit(status, "mlock sub_double_lock");
#else
    sub_double_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(sub_double_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(sub_double_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    mult_double_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (mult_double_lock == NULL);
    tryExit(status, "mmap mult_double_lock");
    status = mlock(mult_double_lock, sizeof (sem_t));
    tryExit(status, "mlock mult_double_lock");
#else
    mult_double_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(mult_double_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(mult_double_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    division_double_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (division_double_lock == NULL);
    tryExit(status, "mmap division_double_lock");
    status = mlock(division_double_lock, sizeof (sem_t));
    tryExit(status, "mlock division_double_lock");
#else
    division_double_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(division_double_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(division_double_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    first_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (first_lock == NULL);
    tryExit(status, "mmap first_lock");
    status = mlock(first_lock, sizeof (sem_t));
    tryExit(status, "mlock first_lock");
#else
    first_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(first_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(first_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    __dupintern__first_a_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (__dupintern__first_a_lock == NULL);
    tryExit(status, "mmap __dupintern__first_a_lock");
    status = mlock(__dupintern__first_a_lock, sizeof (sem_t));
    tryExit(status, "mlock __dupintern__first_a_lock");
#else
    __dupintern__first_a_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(__dupintern__first_a_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(__dupintern__first_a_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    __dupintern__first_a_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (__dupintern__first_a_lock == NULL);
    tryExit(status, "mmap __dupintern__first_a_lock");
    status = mlock(__dupintern__first_a_lock, sizeof (sem_t));
    tryExit(status, "mlock __dupintern__first_a_lock");
#else
    __dupintern__first_a_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(__dupintern__first_a_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(__dupintern__first_a_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    __dupintern__first_a_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (__dupintern__first_a_lock == NULL);
    tryExit(status, "mmap __dupintern__first_a_lock");
    status = mlock(__dupintern__first_a_lock, sizeof (sem_t));
    tryExit(status, "mlock __dupintern__first_a_lock");
#else
    __dupintern__first_a_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(__dupintern__first_a_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(__dupintern__first_a_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl

    // START TaskMgnt/SemInit.tpl
#if 0 == 1
    __dupintern__first_a_lock = (sem_t*) mmap(NULL, sizeof (sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    status = (__dupintern__first_a_lock == NULL);
    tryExit(status, "mmap __dupintern__first_a_lock");
    status = mlock(__dupintern__first_a_lock, sizeof (sem_t));
    tryExit(status, "mlock __dupintern__first_a_lock");
#else
    __dupintern__first_a_lock = (sem_t*) malloc(sizeof (sem_t));
#endif
    status = sem_init(__dupintern__first_a_lock, 0, 0);
    if (status != 0) {
        perror("sem_init(__dupintern__first_a_lock, 0, 0)");
        return status;
    }
    // END TaskMgnt/SemInit.tpl




    return status;
}

void main_clean() {
    int status = 0;

    clean_buffer_double1(&add_b);
    clean_buffer_double1(&sub_c);
    clean_buffer_double1(&mult_d);
    clean_buffer_double1(&division_e);
    clean_buffer_double1(&add_double_f);
    clean_buffer_double1(&sub_double_g);
    clean_buffer_double1(&mult_double_h);
    clean_buffer_double1(&division_double_i);
    clean_buffer_int1(&first_a);
    clean_buffer_int1(&__dupintern__first_a_out4);
    clean_buffer_int1(&__dupintern__first_a_out3);
    clean_buffer_int1(&__dupintern__first_a_out2);
    clean_buffer_int1(&__dupintern__first_a_out1);



    // START TaskMgnt/SemClean.tpl
    if (add_lock != NULL) {
        sem_destroy(add_lock);
#if 0 == 1
        munlock(add_lock, sizeof (sem_t));
        munmap(add_lock, sizeof (sem_t));
#else
        free(add_lock);
#endif
        add_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (sub_lock != NULL) {
        sem_destroy(sub_lock);
#if 0 == 1
        munlock(sub_lock, sizeof (sem_t));
        munmap(sub_lock, sizeof (sem_t));
#else
        free(sub_lock);
#endif
        sub_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (mult_lock != NULL) {
        sem_destroy(mult_lock);
#if 0 == 1
        munlock(mult_lock, sizeof (sem_t));
        munmap(mult_lock, sizeof (sem_t));
#else
        free(mult_lock);
#endif
        mult_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (division_lock != NULL) {
        sem_destroy(division_lock);
#if 0 == 1
        munlock(division_lock, sizeof (sem_t));
        munmap(division_lock, sizeof (sem_t));
#else
        free(division_lock);
#endif
        division_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (add_double_lock != NULL) {
        sem_destroy(add_double_lock);
#if 0 == 1
        munlock(add_double_lock, sizeof (sem_t));
        munmap(add_double_lock, sizeof (sem_t));
#else
        free(add_double_lock);
#endif
        add_double_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (sub_double_lock != NULL) {
        sem_destroy(sub_double_lock);
#if 0 == 1
        munlock(sub_double_lock, sizeof (sem_t));
        munmap(sub_double_lock, sizeof (sem_t));
#else
        free(sub_double_lock);
#endif
        sub_double_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (mult_double_lock != NULL) {
        sem_destroy(mult_double_lock);
#if 0 == 1
        munlock(mult_double_lock, sizeof (sem_t));
        munmap(mult_double_lock, sizeof (sem_t));
#else
        free(mult_double_lock);
#endif
        mult_double_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (division_double_lock != NULL) {
        sem_destroy(division_double_lock);
#if 0 == 1
        munlock(division_double_lock, sizeof (sem_t));
        munmap(division_double_lock, sizeof (sem_t));
#else
        free(division_double_lock);
#endif
        division_double_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (first_lock != NULL) {
        sem_destroy(first_lock);
#if 0 == 1
        munlock(first_lock, sizeof (sem_t));
        munmap(first_lock, sizeof (sem_t));
#else
        free(first_lock);
#endif
        first_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (__dupintern__first_a_lock != NULL) {
        sem_destroy(__dupintern__first_a_lock);
#if 0 == 1
        munlock(__dupintern__first_a_lock, sizeof (sem_t));
        munmap(__dupintern__first_a_lock, sizeof (sem_t));
#else
        free(__dupintern__first_a_lock);
#endif
        __dupintern__first_a_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (__dupintern__first_a_lock != NULL) {
        sem_destroy(__dupintern__first_a_lock);
#if 0 == 1
        munlock(__dupintern__first_a_lock, sizeof (sem_t));
        munmap(__dupintern__first_a_lock, sizeof (sem_t));
#else
        free(__dupintern__first_a_lock);
#endif
        __dupintern__first_a_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (__dupintern__first_a_lock != NULL) {
        sem_destroy(__dupintern__first_a_lock);
#if 0 == 1
        munlock(__dupintern__first_a_lock, sizeof (sem_t));
        munmap(__dupintern__first_a_lock, sizeof (sem_t));
#else
        free(__dupintern__first_a_lock);
#endif
        __dupintern__first_a_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }
    // START TaskMgnt/SemClean.tpl
    if (__dupintern__first_a_lock != NULL) {
        sem_destroy(__dupintern__first_a_lock);
#if 0 == 1
        munlock(__dupintern__first_a_lock, sizeof (sem_t));
        munmap(__dupintern__first_a_lock, sizeof (sem_t));
#else
        free(__dupintern__first_a_lock);
#endif
        __dupintern__first_a_lock = NULL;
        // END TaskMgnt/SemClean.tpl
    }


}

void main_exit(int unused) {
    main_clean();
    exit(0);
}

int main(int argc, char **argv) {
    int status = 0;

    main_init();





    for (size_t __i__ = 0;; ++__i__) {

        // START user-space/SeqCall.tpl
        __coord_add_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_add_v1b();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord___dupintern__first_a_();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_mult_double_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_mult_double_v1b();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_first_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_first_v1b();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_sub_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_sub_v1b();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_mult_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_mult_v1b();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_division_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_division_v1b();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_sum_all_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_sum_all_v1b();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_add_double_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_add_double_v1b();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_division_double_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_division_double_v1b();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_sub_double_v1a();
        // END user-space/SeqCall.tpl

        // START user-space/SeqCall.tpl
        __coord_sub_double_v1b();
        // END user-space/SeqCall.tpl

    }

    main_clean();
    printf("Done, ready to exit\n");
    return status;
}
// END user-space/AppSequential.tpl
