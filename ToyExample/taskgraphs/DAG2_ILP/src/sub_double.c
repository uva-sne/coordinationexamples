#include "ToyExample.h"

void sub_double(int in, double* g){
    printf("input number: %d from task: sub_double\n", in);

    double a=0;
    double b=10;

    for (size_t i = 0; i < NUM_ITER; i++) {
        a -= b;
    }

    printf("Results from sub_double are: %f \n", a);
    *g = 7.0D;
}
