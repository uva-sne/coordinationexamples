#include "ToyExample.h"

void add_double(int in, double* f){
    printf("input number: %d from task: add_double\n", in);

    double a=0;
    double b=10;

    for (size_t i = 0; i < NUM_ITER; i++) {
        a += b;
    }

    printf("Results from add_double are: %f \n", a);
    *f= 6.0D;
}
