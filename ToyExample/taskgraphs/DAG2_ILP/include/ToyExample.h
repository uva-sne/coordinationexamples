#ifndef NUM_ITER
#define NUM_ITER 1000000000
#endif

#include <stdio.h>
#include <stdlib.h>

void first(int* a);
void add(int in, double* b);
void sub(int in, double* c);
void mult(int in, double* d);
void division(int in, double* e);
void add_double(int in, double* f);
void sub_double(int in, double* g);
void mult_double(int in, double* h);
void division_double(int in, double* i);
void sum_all(double mult, double add, double sub, double div, double mult_double, double add_double, double sub_double, double division_double);
void vector_addition(float in, float* f);
void vector_addition_simd(float in, float* f);
