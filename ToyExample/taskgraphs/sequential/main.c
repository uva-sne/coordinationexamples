#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <sched.h>

#include "ToyExample.h"


int first_out=0;
double division_out=0;
double add_out=0;
double sub_out=0;
double mult_out=0;

double add_double_out=0;
double sub_double_out=0;
double mult_double_out=0;
double division_double_out=0;



int main(void) {
    first(&first_out);
    add(first_out, &add_out);
    sub(first_out, &sub_out);
    mult(first_out, &mult_out); //T=1
    division(first_out, &division_out);//T=1
    add_double(sub_out,&add_double_out); //T=11
    sub_double(division_out, &sub_double_out); //T=1
    mult_double(add_out,&mult_double_out); //T=12
    division_double(mult_out,&division_double_out);//T=11
    sum_all(add_double_out, sub_double_out, mult_double_out, division_double_out);//T=22
    return 0;
}
