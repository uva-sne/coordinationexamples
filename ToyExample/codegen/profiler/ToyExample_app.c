
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <semaphore.h>
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <sched.h>
#include <sys/wait.h>

#include "ToyExample.h"
#include "odroid_uart.h"
#include "shared_defs.h"


static inline int sem_wait_nointr(sem_t *sem) {
    while (sem_wait(sem)) {
        if (errno == EINTR) errno = 0;
        else return -1;
    }
    return 0;
}
void main_clean();

#define errExit(msg) { perror(msg); main_clean(); exit(EXIT_FAILURE); }
#define tryExit(var, msg) if(var != 0) {printf("-> %d <-\n",var); errno = var ; errExit(msg) }
#define STRINGIFY(X) #X
#define CONCAT(W,X,Y,Z) W ## X ## Y ## Z

buffer_int1_t first_a;
buffer_int1_t *add_a;
buffer_double1_t add_b;
buffer_int1_t *sub_a;
buffer_double1_t sub_c;
buffer_int1_t *mult_a;
buffer_double1_t mult_d;
buffer_int1_t *division_a;
buffer_double1_t division_e;
buffer_double1_t *add_double_c;
buffer_double1_t add_double_f;
buffer_double1_t *sub_double_e;
buffer_double1_t sub_double_g;
buffer_double1_t *mult_double_b;
buffer_double1_t mult_double_h;
buffer_double1_t *division_double_d;
buffer_double1_t division_double_i;
buffer_double1_t *sum_all_f;
buffer_double1_t *sum_all_g;
buffer_double1_t *sum_all_h;
buffer_double1_t *sum_all_i;
buffer_int1_t *__dupintern__first_a_in;
buffer_int1_t __dupintern__first_a_out4;
buffer_int1_t __dupintern__first_a_out3;
buffer_int1_t __dupintern__first_a_out2;
buffer_int1_t __dupintern__first_a_out1;


sem_t *first_a_lock = NULL;
sem_t *add_b_lock = NULL;
sem_t *sub_c_lock = NULL;
sem_t *mult_d_lock = NULL;
sem_t *division_e_lock = NULL;
sem_t *add_double_f_lock = NULL;
sem_t *sub_double_g_lock = NULL;
sem_t *mult_double_h_lock = NULL;
sem_t *division_double_i_lock = NULL;
sem_t *__dupintern__first_a_out4_lock = NULL;
sem_t *__dupintern__first_a_out3_lock = NULL;
sem_t *__dupintern__first_a_out2_lock = NULL;
sem_t *__dupintern__first_a_out1_lock = NULL;



void __coord_first_() {
    int status;



    int a = 0;
int  __bkinit__a = a;

    first(&a);

if(a != __bkinit__a)
    push_int1(&first_a, a);



if(a != __bkinit__a)
    sem_post(first_a_lock);

}

void __coord_add_() {
    int status;


    status = sem_wait_nointr(__dupintern__first_a_out1_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_out1_lock)");
        return;
    }


    int a = 0;
double b = 0;
double  __bkinit__b = b;
pop_int1(add_a, &a);

    add(a, &b);

if(b != __bkinit__b)
    push_double1(&add_b, b);



if(b != __bkinit__b)
    sem_post(add_b_lock);

}

void __coord_sub_() {
    int status;


    status = sem_wait_nointr(__dupintern__first_a_out2_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_out2_lock)");
        return;
    }


    int a = 0;
double c = 0;
double  __bkinit__c = c;
pop_int1(sub_a, &a);

    sub(a, &c);

if(c != __bkinit__c)
    push_double1(&sub_c, c);



if(c != __bkinit__c)
    sem_post(sub_c_lock);

}

void __coord_mult_() {
    int status;


    status = sem_wait_nointr(__dupintern__first_a_out3_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_out3_lock)");
        return;
    }


    int a = 0;
double d = 0;
double  __bkinit__d = d;
pop_int1(mult_a, &a);

    mult(a, &d);

if(d != __bkinit__d)
    push_double1(&mult_d, d);



if(d != __bkinit__d)
    sem_post(mult_d_lock);

}

void __coord_division_() {
    int status;


    status = sem_wait_nointr(__dupintern__first_a_out4_lock);
    if(status != 0) {
        perror("sem_wait_nointr(__dupintern__first_a_out4_lock)");
        return;
    }


    int a = 0;
double e = 0;
double  __bkinit__e = e;
pop_int1(division_a, &a);

    division(a, &e);

if(e != __bkinit__e)
    push_double1(&division_e, e);



if(e != __bkinit__e)
    sem_post(division_e_lock);

}

void __coord_add_double_() {
    int status;


    status = sem_wait_nointr(sub_c_lock);
    if(status != 0) {
        perror("sem_wait_nointr(sub_c_lock)");
        return;
    }


    double c = 0;
double f = 0;
double  __bkinit__f = f;
pop_double1(add_double_c, &c);

    add_double(c, &f);

if(f != __bkinit__f)
    push_double1(&add_double_f, f);



if(f != __bkinit__f)
    sem_post(add_double_f_lock);

}

void __coord_sub_double_() {
    int status;


    status = sem_wait_nointr(division_e_lock);
    if(status != 0) {
        perror("sem_wait_nointr(division_e_lock)");
        return;
    }


    double e = 0;
double g = 0;
double  __bkinit__g = g;
pop_double1(sub_double_e, &e);

    sub_double(e, &g);

if(g != __bkinit__g)
    push_double1(&sub_double_g, g);



if(g != __bkinit__g)
    sem_post(sub_double_g_lock);

}

void __coord_mult_double_() {
    int status;


    status = sem_wait_nointr(add_b_lock);
    if(status != 0) {
        perror("sem_wait_nointr(add_b_lock)");
        return;
    }


    double b = 0;
double h = 0;
double  __bkinit__h = h;
pop_double1(mult_double_b, &b);

    mult_double(b, &h);

if(h != __bkinit__h)
    push_double1(&mult_double_h, h);



if(h != __bkinit__h)
    sem_post(mult_double_h_lock);

}

void __coord_division_double_() {
    int status;


    status = sem_wait_nointr(mult_d_lock);
    if(status != 0) {
        perror("sem_wait_nointr(mult_d_lock)");
        return;
    }


    double d = 0;
double i = 0;
double  __bkinit__i = i;
pop_double1(division_double_d, &d);

    division_double(d, &i);

if(i != __bkinit__i)
    push_double1(&division_double_i, i);



if(i != __bkinit__i)
    sem_post(division_double_i_lock);

}

void __coord_sum_all_() {
    int status;


    status = sem_wait_nointr(add_double_f_lock);
    if(status != 0) {
        perror("sem_wait_nointr(add_double_f_lock)");
        return;
    }

    status = sem_wait_nointr(sub_double_g_lock);
    if(status != 0) {
        perror("sem_wait_nointr(sub_double_g_lock)");
        return;
    }

    status = sem_wait_nointr(mult_double_h_lock);
    if(status != 0) {
        perror("sem_wait_nointr(mult_double_h_lock)");
        return;
    }

    status = sem_wait_nointr(division_double_i_lock);
    if(status != 0) {
        perror("sem_wait_nointr(division_double_i_lock)");
        return;
    }


    double f = 0;
double g = 0;
double h = 0;
double i = 0;
pop_double1(sum_all_f, &f);
pop_double1(sum_all_g, &g);
pop_double1(sum_all_h, &h);
pop_double1(sum_all_i, &i);

    sum_all(f, g, h, i);



}

void __coord___dupintern__first_a_() {
    int status;


    status = sem_wait_nointr(first_a_lock);
    if(status != 0) {
        perror("sem_wait_nointr(first_a_lock)");
        return;
    }


    int in = 0;
int out4 = 0;
int  __bkinit__out4 = out4;
int out3 = 0;
int  __bkinit__out3 = out3;
int out2 = 0;
int  __bkinit__out2 = out2;
int out1 = 0;
int  __bkinit__out1 = out1;
pop_int1(__dupintern__first_a_in, &in);

    out4 = in;
out3 = in;
out2 = in;
out1 = in;


if(out4 != __bkinit__out4)
    push_int1(&__dupintern__first_a_out4, out4);

if(out3 != __bkinit__out3)
    push_int1(&__dupintern__first_a_out3, out3);

if(out2 != __bkinit__out2)
    push_int1(&__dupintern__first_a_out2, out2);

if(out1 != __bkinit__out1)
    push_int1(&__dupintern__first_a_out1, out1);



if(out4 != __bkinit__out4)
    sem_post(__dupintern__first_a_out4_lock);

if(out3 != __bkinit__out3)
    sem_post(__dupintern__first_a_out3_lock);

if(out2 != __bkinit__out2)
    sem_post(__dupintern__first_a_out2_lock);

if(out1 != __bkinit__out1)
    sem_post(__dupintern__first_a_out1_lock);

}


void main_exit(int);
int main_init() {
    int status = 0;

    signal(SIGINT, main_exit);
    signal(SIGILL, main_exit);
    signal(SIGABRT, main_exit);
    signal(SIGFPE, main_exit);
    signal(SIGSEGV, main_exit);
    signal(SIGTERM, main_exit);
    signal(SIGKILL, main_exit);
    signal(SIGSTOP, main_exit);
    signal(SIGTSTP, main_exit);

    	init_buffer_int1(&first_a);
	init_buffer_double1(&add_b);
	init_buffer_double1(&sub_c);
	init_buffer_double1(&mult_d);
	init_buffer_double1(&division_e);
	init_buffer_double1(&add_double_f);
	init_buffer_double1(&sub_double_g);
	init_buffer_double1(&mult_double_h);
	init_buffer_double1(&division_double_i);
	init_buffer_int1(&__dupintern__first_a_out4);
	init_buffer_int1(&__dupintern__first_a_out3);
	init_buffer_int1(&__dupintern__first_a_out2);
	init_buffer_int1(&__dupintern__first_a_out1);
	add_a = &__dupintern__first_a_out1;
	sub_a = &__dupintern__first_a_out2;
	mult_a = &__dupintern__first_a_out3;
	division_a = &__dupintern__first_a_out4;
	add_double_c = &sub_c;
	sub_double_e = &division_e;
	mult_double_b = &add_b;
	division_double_d = &mult_d;
	sum_all_f = &add_double_f;
	sum_all_g = &sub_double_g;
	sum_all_h = &mult_double_h;
	sum_all_i = &division_double_i;
	__dupintern__first_a_in = &first_a;



#if 0 == 1
first_a_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (first_a_lock == NULL);
tryExit(status, "mmap first_a_lock");
status = mlock(first_a_lock, sizeof(sem_t));
tryExit(status, "mlock first_a_lock");
#else
first_a_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(first_a_lock, 0, 0);
if(status != 0) {
    perror("sem_init(first_a_lock, 0, 0)");
    return status;
}

#if 0 == 1
add_b_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (add_b_lock == NULL);
tryExit(status, "mmap add_b_lock");
status = mlock(add_b_lock, sizeof(sem_t));
tryExit(status, "mlock add_b_lock");
#else
add_b_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(add_b_lock, 0, 0);
if(status != 0) {
    perror("sem_init(add_b_lock, 0, 0)");
    return status;
}

#if 0 == 1
sub_c_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (sub_c_lock == NULL);
tryExit(status, "mmap sub_c_lock");
status = mlock(sub_c_lock, sizeof(sem_t));
tryExit(status, "mlock sub_c_lock");
#else
sub_c_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(sub_c_lock, 0, 0);
if(status != 0) {
    perror("sem_init(sub_c_lock, 0, 0)");
    return status;
}

#if 0 == 1
mult_d_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (mult_d_lock == NULL);
tryExit(status, "mmap mult_d_lock");
status = mlock(mult_d_lock, sizeof(sem_t));
tryExit(status, "mlock mult_d_lock");
#else
mult_d_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(mult_d_lock, 0, 0);
if(status != 0) {
    perror("sem_init(mult_d_lock, 0, 0)");
    return status;
}

#if 0 == 1
division_e_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (division_e_lock == NULL);
tryExit(status, "mmap division_e_lock");
status = mlock(division_e_lock, sizeof(sem_t));
tryExit(status, "mlock division_e_lock");
#else
division_e_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(division_e_lock, 0, 0);
if(status != 0) {
    perror("sem_init(division_e_lock, 0, 0)");
    return status;
}

#if 0 == 1
add_double_f_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (add_double_f_lock == NULL);
tryExit(status, "mmap add_double_f_lock");
status = mlock(add_double_f_lock, sizeof(sem_t));
tryExit(status, "mlock add_double_f_lock");
#else
add_double_f_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(add_double_f_lock, 0, 0);
if(status != 0) {
    perror("sem_init(add_double_f_lock, 0, 0)");
    return status;
}

#if 0 == 1
sub_double_g_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (sub_double_g_lock == NULL);
tryExit(status, "mmap sub_double_g_lock");
status = mlock(sub_double_g_lock, sizeof(sem_t));
tryExit(status, "mlock sub_double_g_lock");
#else
sub_double_g_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(sub_double_g_lock, 0, 0);
if(status != 0) {
    perror("sem_init(sub_double_g_lock, 0, 0)");
    return status;
}

#if 0 == 1
mult_double_h_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (mult_double_h_lock == NULL);
tryExit(status, "mmap mult_double_h_lock");
status = mlock(mult_double_h_lock, sizeof(sem_t));
tryExit(status, "mlock mult_double_h_lock");
#else
mult_double_h_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(mult_double_h_lock, 0, 0);
if(status != 0) {
    perror("sem_init(mult_double_h_lock, 0, 0)");
    return status;
}

#if 0 == 1
division_double_i_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (division_double_i_lock == NULL);
tryExit(status, "mmap division_double_i_lock");
status = mlock(division_double_i_lock, sizeof(sem_t));
tryExit(status, "mlock division_double_i_lock");
#else
division_double_i_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(division_double_i_lock, 0, 0);
if(status != 0) {
    perror("sem_init(division_double_i_lock, 0, 0)");
    return status;
}

#if 0 == 1
__dupintern__first_a_out4_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (__dupintern__first_a_out4_lock == NULL);
tryExit(status, "mmap __dupintern__first_a_out4_lock");
status = mlock(__dupintern__first_a_out4_lock, sizeof(sem_t));
tryExit(status, "mlock __dupintern__first_a_out4_lock");
#else
__dupintern__first_a_out4_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(__dupintern__first_a_out4_lock, 0, 0);
if(status != 0) {
    perror("sem_init(__dupintern__first_a_out4_lock, 0, 0)");
    return status;
}

#if 0 == 1
__dupintern__first_a_out3_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (__dupintern__first_a_out3_lock == NULL);
tryExit(status, "mmap __dupintern__first_a_out3_lock");
status = mlock(__dupintern__first_a_out3_lock, sizeof(sem_t));
tryExit(status, "mlock __dupintern__first_a_out3_lock");
#else
__dupintern__first_a_out3_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(__dupintern__first_a_out3_lock, 0, 0);
if(status != 0) {
    perror("sem_init(__dupintern__first_a_out3_lock, 0, 0)");
    return status;
}

#if 0 == 1
__dupintern__first_a_out2_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (__dupintern__first_a_out2_lock == NULL);
tryExit(status, "mmap __dupintern__first_a_out2_lock");
status = mlock(__dupintern__first_a_out2_lock, sizeof(sem_t));
tryExit(status, "mlock __dupintern__first_a_out2_lock");
#else
__dupintern__first_a_out2_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(__dupintern__first_a_out2_lock, 0, 0);
if(status != 0) {
    perror("sem_init(__dupintern__first_a_out2_lock, 0, 0)");
    return status;
}

#if 0 == 1
__dupintern__first_a_out1_lock = (sem_t*)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
status = (__dupintern__first_a_out1_lock == NULL);
tryExit(status, "mmap __dupintern__first_a_out1_lock");
status = mlock(__dupintern__first_a_out1_lock, sizeof(sem_t));
tryExit(status, "mlock __dupintern__first_a_out1_lock");
#else
__dupintern__first_a_out1_lock = (sem_t*)malloc(sizeof(sem_t));
#endif
status = sem_init(__dupintern__first_a_out1_lock, 0, 0);
if(status != 0) {
    perror("sem_init(__dupintern__first_a_out1_lock, 0, 0)");
    return status;
}




    return status;
}

void main_clean() {
    int status = 0;

    	clean_buffer_int1(&first_a);
	clean_buffer_double1(&add_b);
	clean_buffer_double1(&sub_c);
	clean_buffer_double1(&mult_d);
	clean_buffer_double1(&division_e);
	clean_buffer_double1(&add_double_f);
	clean_buffer_double1(&sub_double_g);
	clean_buffer_double1(&mult_double_h);
	clean_buffer_double1(&division_double_i);
	clean_buffer_int1(&__dupintern__first_a_out4);
	clean_buffer_int1(&__dupintern__first_a_out3);
	clean_buffer_int1(&__dupintern__first_a_out2);
	clean_buffer_int1(&__dupintern__first_a_out1);



if(first_a_lock != NULL) {
    sem_destroy(first_a_lock);
#if 0 == 1
    munlock(first_a_lock, sizeof(sem_t));
    munmap(first_a_lock, sizeof(sem_t));
#else
    free(first_a_lock);
#endif
    first_a_lock = NULL;
}
if(add_b_lock != NULL) {
    sem_destroy(add_b_lock);
#if 0 == 1
    munlock(add_b_lock, sizeof(sem_t));
    munmap(add_b_lock, sizeof(sem_t));
#else
    free(add_b_lock);
#endif
    add_b_lock = NULL;
}
if(sub_c_lock != NULL) {
    sem_destroy(sub_c_lock);
#if 0 == 1
    munlock(sub_c_lock, sizeof(sem_t));
    munmap(sub_c_lock, sizeof(sem_t));
#else
    free(sub_c_lock);
#endif
    sub_c_lock = NULL;
}
if(mult_d_lock != NULL) {
    sem_destroy(mult_d_lock);
#if 0 == 1
    munlock(mult_d_lock, sizeof(sem_t));
    munmap(mult_d_lock, sizeof(sem_t));
#else
    free(mult_d_lock);
#endif
    mult_d_lock = NULL;
}
if(division_e_lock != NULL) {
    sem_destroy(division_e_lock);
#if 0 == 1
    munlock(division_e_lock, sizeof(sem_t));
    munmap(division_e_lock, sizeof(sem_t));
#else
    free(division_e_lock);
#endif
    division_e_lock = NULL;
}
if(add_double_f_lock != NULL) {
    sem_destroy(add_double_f_lock);
#if 0 == 1
    munlock(add_double_f_lock, sizeof(sem_t));
    munmap(add_double_f_lock, sizeof(sem_t));
#else
    free(add_double_f_lock);
#endif
    add_double_f_lock = NULL;
}
if(sub_double_g_lock != NULL) {
    sem_destroy(sub_double_g_lock);
#if 0 == 1
    munlock(sub_double_g_lock, sizeof(sem_t));
    munmap(sub_double_g_lock, sizeof(sem_t));
#else
    free(sub_double_g_lock);
#endif
    sub_double_g_lock = NULL;
}
if(mult_double_h_lock != NULL) {
    sem_destroy(mult_double_h_lock);
#if 0 == 1
    munlock(mult_double_h_lock, sizeof(sem_t));
    munmap(mult_double_h_lock, sizeof(sem_t));
#else
    free(mult_double_h_lock);
#endif
    mult_double_h_lock = NULL;
}
if(division_double_i_lock != NULL) {
    sem_destroy(division_double_i_lock);
#if 0 == 1
    munlock(division_double_i_lock, sizeof(sem_t));
    munmap(division_double_i_lock, sizeof(sem_t));
#else
    free(division_double_i_lock);
#endif
    division_double_i_lock = NULL;
}
if(__dupintern__first_a_out4_lock != NULL) {
    sem_destroy(__dupintern__first_a_out4_lock);
#if 0 == 1
    munlock(__dupintern__first_a_out4_lock, sizeof(sem_t));
    munmap(__dupintern__first_a_out4_lock, sizeof(sem_t));
#else
    free(__dupintern__first_a_out4_lock);
#endif
    __dupintern__first_a_out4_lock = NULL;
}
if(__dupintern__first_a_out3_lock != NULL) {
    sem_destroy(__dupintern__first_a_out3_lock);
#if 0 == 1
    munlock(__dupintern__first_a_out3_lock, sizeof(sem_t));
    munmap(__dupintern__first_a_out3_lock, sizeof(sem_t));
#else
    free(__dupintern__first_a_out3_lock);
#endif
    __dupintern__first_a_out3_lock = NULL;
}
if(__dupintern__first_a_out2_lock != NULL) {
    sem_destroy(__dupintern__first_a_out2_lock);
#if 0 == 1
    munlock(__dupintern__first_a_out2_lock, sizeof(sem_t));
    munmap(__dupintern__first_a_out2_lock, sizeof(sem_t));
#else
    free(__dupintern__first_a_out2_lock);
#endif
    __dupintern__first_a_out2_lock = NULL;
}
if(__dupintern__first_a_out1_lock != NULL) {
    sem_destroy(__dupintern__first_a_out1_lock);
#if 0 == 1
    munlock(__dupintern__first_a_out1_lock, sizeof(sem_t));
    munmap(__dupintern__first_a_out1_lock, sizeof(sem_t));
#else
    free(__dupintern__first_a_out1_lock);
#endif
    __dupintern__first_a_out1_lock = NULL;
}


}

void main_exit(int unused) {
    main_clean();
    exit(0);
}

int main(int argc, char **argv)
{
    int status = 0;

    main_init();

    unsigned char *buffer = "TEST";
    int fd = openport();
    send_uart(buffer, fd);

    for(;;) {
        buffer =  "First Component Start \n";
        send_uart(buffer, fd);
        __coord_first_();
        buffer = "First Component Stop \n";
        send_uart(buffer, fd);

        __coord___dupintern__first_a_();

        buffer = "Add Component Start \n";
        send_uart(buffer, fd);
        __coord_add_();
        buffer = "Add Component Stop \n";
        send_uart(buffer, fd);

        buffer = "Sub Component Start \n";
        send_uart(buffer, fd);
        __coord_sub_();
        buffer = "Sub Component Stop \n";
        send_uart(buffer, fd);

        buffer = "Mult Component Start \n";
        send_uart(buffer, fd);
        __coord_mult_();
        buffer = "Mult Component Stop \n";
        send_uart(buffer, fd);

        buffer = "Division Component Start \n";
        send_uart(buffer, fd);
        __coord_division_();
        buffer = "Division Component Stop \n";
        send_uart(buffer, fd);

        buffer = "Mult_double Component Start \n";
        send_uart(buffer, fd);
        __coord_mult_double_();
        buffer = "Mult_double Component Stop \n";
        send_uart(buffer, fd);

        buffer = "Add_double Component Start \n";
        send_uart(buffer, fd);
        __coord_add_double_();
        buffer = "Add_double Component Stop \n";
        send_uart(buffer, fd);

        buffer = "Div_double Component Start \n";
        send_uart(buffer, fd);
        __coord_division_double_();
        buffer = "Div_double Component Stop \n";
        send_uart(buffer, fd);

        buffer = "Sub_double Component Start \n";
        send_uart(buffer, fd);
        __coord_sub_double_();
        buffer = "Sub_double Component Stop \n";
        send_uart(buffer, fd);

        buffer = "Sum_all Component Start \n";
        send_uart(buffer, fd);
        __coord_sum_all_();
        buffer = "Sum_all Component Stop \n";
        send_uart(buffer, fd);

    }

    main_clean();
    printf("Done, ready to exit\n");
    return status;
}
