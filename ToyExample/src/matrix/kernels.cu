R"CRAWMARKER(
#define NUM_ROWS_COLS 10
typedef struct _IntMatrix matrix_int_t;

struct _IntMatrix {
    int cell[NUM_ROWS_COLS][NUM_ROWS_COLS];
};
__kernel void first_gpu(__global matrix_int_t* a) {
    // Thread identifiers
    const int i = get_global_id(0); // Row ID of C (0..num_rows_cols
    const int j = get_global_id(1); // Col ID of C (0..num_rows_cols
    a->cell[i][j] = 1;
}

__kernel void add_gpu(const __global matrix_int_t* in, __global matrix_int_t *out) {
    // Thread identifiers
    const int i = get_global_id(0); // Row ID of C (0..num_rows_cols
    const int j = get_global_id(1); // Col ID of C (0..num_rows_cols
    out->cell[i][j] = in->cell[i][j] + 42;
}

__kernel void division_gpu(const __global matrix_int_t* in, __global matrix_int_t *out) {
    // Thread identifiers
    const int i = get_global_id(0); // Row ID of C (0..num_rows_cols
    const int j = get_global_id(1); // Col ID of C (0..num_rows_cols
    const float stuff = 1.12;
    out->cell[i][j] = in->cell[i][j] / stuff;
}

__kernel void mult_gpu(const __global matrix_int_t* in, __global matrix_int_t *out) {
    // Thread identifiers
    const int i = get_global_id(0); // Row ID of C (0..num_rows_cols
    const int j = get_global_id(1); // Col ID of C (0..num_rows_cols
    out->cell[i][j] = in->cell[i][j] * 42;
}

__kernel void sub_gpu(const __global matrix_int_t* in, __global matrix_int_t *out) {
    // Thread identifiers
    const int i = get_global_id(0); // Row ID of C (0..num_rows_cols
    const int j = get_global_id(1); // Col ID of C (0..num_rows_cols
    out->cell[i][j] = in->cell[i][j] - 6;
}

__kernel void square_gpu(const int num_rows_cols, const __global matrix_int_t* in, __global matrix_int_t *out) {
    // Thread identifiers
    const int i = get_global_id(0); // Row ID of C (0..num_rows_cols
    const int j = get_global_id(1); // Col ID of C (0..num_rows_cols
    
    // Compute a single element (loop over num_rows_cols
    long acc = 0;
    for (int k = 0; k < num_rows_cols ; k++) {
        acc += in->cell[k][i] * in->cell[j][k];
    }
 
    // Store the result
    out->cell[i][j] = acc;
}

__kernel void mult_matrix_gpu(const int num_rows_cols, const __global matrix_int_t* a, const __global matrix_int_t* b, __global matrix_int_t *out) {
    // Thread identifiers
    const int i = get_global_id(0); // Row ID of C (0..num_rows_cols
    const int j = get_global_id(1); // Col ID of C (0..num_rows_cols
    
    // Compute a single element (loop over num_rows_cols
    long acc = 0;
    for (int k = 0; k < num_rows_cols ; k++) {
        acc += a->cell[k][i] * b->cell[j][k];
    }
 
    // Store the result
    out->cell[i][j] = acc;
}

__kernel void transpose_gpu(const __global matrix_int_t* in, __global matrix_int_t *out) {
    // Thread identifiers
    const int i = get_global_id(0); // Row ID of C (0..num_rows_cols
    const int j = get_global_id(1); // Col ID of C (0..num_rows_cols
    out->cell[j][i] = in->cell[i][j];
}

__kernel void sum_all_gpu(const __global matrix_int_t* f, const __global matrix_int_t* g, const __global matrix_int_t* h, __global matrix_int_t *out) {
    // Thread identifiers
    const int i = get_global_id(0); // Row ID of C (0..num_rows_cols
    const int j = get_global_id(1); // Col ID of C (0..num_rows_cols
    out->cell[j][i] = f->cell[i][j]+g->cell[i][j]+h->cell[i][j];
}
)CRAWMARKER"