#include "ToyExampleMatrix.h"

void transpose_matrix(matrix_int_t in, matrix_int_t* out){
    printf("transpose_matrix \n");
    
    for(int i=NUM_ROWS_COLS-1 ; i >= 0 ; --i) {
        for(int j=NUM_ROWS_COLS-1 ; j >= 0 ; --j) {
            out->cell[j][i] = in.cell[i][j];
        }
    }
}

void transpose_matrix_v1c(matrix_int_t in, matrix_int_t* out) {
    printf("transpose_matrix_v1c \n");
    int num_rows = NUM_ROWS_COLS;
    int num_cols = NUM_ROWS_COLS;
    
    cl_program program = get_program();
    cl_command_queue queue = get_queue();
    cl_context context = get_context();
    
    cl_mem bufferin = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(matrix_int_t), (void*)&in, NULL);
    cl_mem bufferout = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(matrix_int_t), (void*)out, NULL);
    
    cl_kernel kernel = clCreateKernel(program, "transpose_gpu", NULL);
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&bufferin);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&bufferout);

    const size_t global[2] = { num_rows, num_cols };
    cl_event event = NULL;
    clEnqueueNDRangeKernel(queue, kernel, 2, NULL, global, NULL, 0, NULL, &event);
    clWaitForEvents(1, &event);
    
    clEnqueueReadBuffer(queue, bufferout, CL_TRUE, 0, sizeof(matrix_int_t), (void*)out, 0, NULL, NULL);
    
    clReleaseMemObject(bufferin);
    clReleaseMemObject(bufferout);
}
