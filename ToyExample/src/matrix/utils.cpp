#include "ToyExampleMatrix.h"

_IntMatrix::_IntMatrix() {
    for(int i=NUM_ROWS_COLS-1 ; i >= 0 ; --i ) {
        for(int j=NUM_ROWS_COLS-1 ; j >= 0 ; --j ) {
            cell[i][j] = -1;
        }
    }
}

_IntMatrix::_IntMatrix(const _IntMatrix& rhs) {
    *this = rhs;
}

_IntMatrix& _IntMatrix::operator=(_IntMatrix &b) {
    for(int i=NUM_ROWS_COLS-1 ; i >= 0 ; --i ) {
        for(int j=NUM_ROWS_COLS-1 ; j >= 0 ; --j ) {
            cell[i][j] = b.cell[i][j];
        }
    }
    return *this;
}
_IntMatrix& _IntMatrix::operator=(const _IntMatrix &b) {
    for(int i=NUM_ROWS_COLS-1 ; i >= 0 ; --i ) {
        for(int j=NUM_ROWS_COLS-1 ; j >= 0 ; --j ) {
            cell[i][j] = b.cell[i][j];
        }
    }
    return *this;
}

void _IntMatrix::display() {
    for(int i=0 ; i < NUM_ROWS_COLS ; ++i) {
        for(int j=0 ; j < NUM_ROWS_COLS ; ++j) {
            printf("%d ", cell[i][j]);
        }
        printf("\n");
    }
}

bool operator==(const matrix_int_t &a, const matrix_int_t &b) {
    for(int i=NUM_ROWS_COLS-1 ; i >= 0 ; --i ) {
        for(int j=NUM_ROWS_COLS-1 ; j >= 0 ; --j ) {
            if(a.cell[i][j] != b.cell[i][j])
                return false;
        }
    }
    return true;
}

bool operator!=(const matrix_int_t &a, const matrix_int_t &b) {
    bool t = !(a == b);
    return t;
}
