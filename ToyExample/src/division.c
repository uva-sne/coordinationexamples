#include "ToyExample.h"

void division(int in, double* e){
    printf("input number: %d from task: division\n", in);

    int a=1029430;
    int b=2;

    for (size_t i = 0; i < NUM_ITER; i++) {
        a /= b;
    }

    printf("Results from division are: %d \n", a);
    *e= 5.0D;
}
