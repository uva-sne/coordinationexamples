#include "ToyExample.h"

void add(int in, double* out){
    printf("input number: %d from task: add\n", in);

    int a=0;
    int b=10;

    for (size_t i = 0; i < NUM_ITER; i++) {
        a += b;
    }

    printf("Results from add are: %d \n", a);
    *out = 2.0D;
}
