#include "powprofiler.hpp"

void powprof_start(const char *componentname, const char *componentversion, int mapping) {
    std::cout << "Enjoy " << componentname << " - " << componentversion << std::endl;
}
void powprof_stop(const char *componentname, const char *componentversion, int mapping) {
    std::cout << "Doh " << componentname << " - " << componentversion << std::endl;
}
void powprof_samp() {
    
}