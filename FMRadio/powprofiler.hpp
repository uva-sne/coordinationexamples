#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string>

void powprof_start(const char *componentname, const char *componentversion, int mapping);
void powprof_stop(const char *componentname, const char *componentversion, int mapping);
void powprof_samp();
