#include "FMRadio_7bands.h"

int main(int argc, char **argv) {
    fm7b_init();
    
    FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
        fm7b_float_one_source();
        //decimate 4 samples after outputting 1
        fm7b_low_pass_filter_4samp();
        fm7b_fmdemodulator();
        fm7b_split1_duplicate();
        FOR(uint32_t, i, 0, <, TheGlobal_eqBands, i++)
            fm7b_split2_duplicate(i);
                fm7b_lowpass_filter(i);
                fm7b_lowpass_filter(TheGlobal_eqBands+i);
            fm7b_join2_round_robin(i);

            fm7b_subtracter(i);
            fm7b_amplify(i);
        ENDFOR
        fm7b_join1_round_robin();
        fm7b_equalizer();
        fm7b_float_printer();
    ENDFOR
    return EXIT_SUCCESS;
}
