#include "ForkJoin.h"
#include "time.h"

static int counter = 0;

void fork_fn(int *out){
    *out = counter;
    fprintf(stderr, "[%lu] fork sending %i\n", (unsigned long) time(NULL), counter++);
}
void left_fn(int in, int *out){
    *out = 2 * in;
    fprintf(stderr, "[%lu] left got %i sending %i\n", (unsigned long) time(NULL), in, *out);
}
void right_fn(int in, int *out){
    *out = -in;
    fprintf(stderr, "[%lu] right got %i sending %i\n", (unsigned long) time(NULL), in, *out);
}
void join_fn(int left_in, int right_in) {
    fprintf(stderr, "[%lu] join got %i and %i\n", (unsigned long) time(NULL), left_in, right_in);
}

void wait_key_stop() {
    char c;
    do { c = getchar(); } while(c != 'q');
}