#include <stdio.h>
#include <stdlib.h>

void fork_fn(int *out);
void left_fn(int in, int *out);
void right_fn(int in, int *out);
void join_fn(int left_in, int right_in);
void wait_key_stop();