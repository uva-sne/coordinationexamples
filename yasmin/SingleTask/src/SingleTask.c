#include "SingleTask.h"

void single_task(){
    fprintf(stderr, "executing single task ...\n");
}

void wait_key_stop() {
    char c;
    do { c = getchar(); } while(c != 'q');
}