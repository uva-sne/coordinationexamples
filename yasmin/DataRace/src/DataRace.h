#include <stdio.h>
#include <stdlib.h>

extern "C"
{
    void reader(int *value);
    void printer(int value);
    void writer(int value);
    void waitKeyStop();
}