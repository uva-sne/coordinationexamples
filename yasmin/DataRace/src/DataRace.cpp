#include "DataRace.h"
#include "yasmin_api.h"

int persisted_int = 0;

/**
 * Race condition example
 * The "persisted_int" is shared between iterations of the task set without any form of barrier or other exclusivity means
 * However, this should be safe, as there is no pipelining (right?)
 * EXPECTED BEHAVIOR is to never print "-1", and instead print increasing numbers
 */

void reader(int* value) {
    // reader task -> reads the persisted value
    *value = persisted_int;
    fprintf(stderr, "r: read %i from previous iteration\n", *value);
}

void printer(int value) {
    // this just exist so reader and writer aren't put on the same thread
    fprintf(stderr, "p: got %i from reader\n", value);
}

void writer(int value){
    // writer task -> has a WCET of 1s but exceeds it
    persisted_int = -1; // illegal value to make it more obvious
    fprintf(stderr, "w: writing illegal value %i\n", persisted_int);
    value++;
    yas_utils_nanosleep(3LL * 1000LL * 1000LL * 1000LL); // 3 seconds
    persisted_int = value;
    fprintf(stderr, "w: writing %i...\n", value);
}

void waitKeyStop() {
    char c;
    do { c = getchar(); } while(c != 'q');
}